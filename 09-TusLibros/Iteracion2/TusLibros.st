!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 13:59:26'!
test01CannotCheckOutAnEmptyCart
	|mp|
	
	mp := MerchantProcessor simulatingValidTransactions.

	self should: [self assert: (Cashier for: self emptyShoppingCart withCreditCard: self validCreditCard using: mp) checkOut]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: Cashier emptyCartErrorDescription.].
	! !

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 13:58:24'!
test02CanCheckOutACartWithOneItem
	|cashier mp transactionID|
	mp := MerchantProcessor simulatingValidTransactions.

	cashier := Cashier for: self shoppingCartWithOneItem withCreditCard: self validCreditCard using: mp.
	transactionID := cashier checkOut.
	
	self assert: (mp lastPurchaseWasForAmount: 31).
	self assert: transactionID equals: 1.! !

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 13:58:06'!
test03CanCheckOutACartWithMultipleItem
	|cashier mp transactionID|
	mp := MerchantProcessor simulatingValidTransactions.
	
	cashier := Cashier for: self shoppingCartWithMultipleItems withCreditCard: self validCreditCard using: mp .
	
	transactionID := cashier checkOut.
	
	self assert: (mp lastPurchaseWasForAmount: 51).
	self assert: transactionID equals: 1.! !

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 13:58:50'!
test04CannotCheckOutForAnExpiredCreditCard
	|cashier mp|
	mp := MerchantProcessor simulatingValidTransactions.
	
	
	cashier := Cashier for: self shoppingCartWithOneItem withCreditCard: self expiredCreditCard using: mp.
	
	self should: [cashier checkOut]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: Cashier expiredCardErrorDescription.
		self deny: cashier hasSold].
! !

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 14:00:50'!
test05CannotCheckOutIfCreditCardWasMarkedAsStolen
	|cashier mp |
	
	mp := MerchantProcessor simulatingStolenCardDetection.
	
	cashier := (Cashier for: self shoppingCartWithOneItem withCreditCard: self validCreditCard using: mp).
	
	self should: [cashier checkOut]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: MerchantProcessor stolenCardErrorDescription.
		self deny: cashier hasSold].
! !

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 14:02:02'!
test06CannotCheckOutIfDebitIsInsufficient
	|cashier mp |
	
	mp := MerchantProcessor simulatingInsuficientCreditDetection.
	
	cashier := (Cashier for: self shoppingCartWithOneItem withCreditCard: self validCreditCard using: mp).
	
	self should: [cashier checkOut]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: MerchantProcessor insuficientDebitErrorDescription.
		self deny: cashier hasSold].
! !

!CashierTest methodsFor: 'tests' stamp: 'FA 11/6/2023 14:02:17'!
test07MultipleCheckOutsHaveDifferentTransactionIds
	|cashier1 cashier2 mp transactionID_1 transactionID_2|
	
	mp := MerchantProcessor simulatingValidTransactions.
		
	cashier1 := (Cashier for: self shoppingCartWithOneItem withCreditCard: self validCreditCard using: mp).
	cashier2 := (Cashier for: self shoppingCartWithMultipleItems withCreditCard: self validCreditCard using: mp).

	transactionID_1 := cashier1 checkOut.
	transactionID_2 := cashier2 checkOut.
	
	self deny: transactionID_1 = transactionID_2.! !


!CashierTest methodsFor: 'testing abstraction' stamp: 'FA 11/6/2023 14:06:53'!
catalogWith2Items
	|catalogWithPrices |
		
	catalogWithPrices := Dictionary new.
	catalogWithPrices add: 'book1'->31.
	catalogWithPrices add: 'book2'->10.	
	
	^catalogWithPrices.! !

!CashierTest methodsFor: 'testing abstraction' stamp: 'FA 11/5/2023 16:16:18'!
emptyShoppingCart
	|catalog|
	
	catalog := OrderedCollection new.
	
	^ShoppingCart forCatalog: catalog.! !

!CashierTest methodsFor: 'testing abstraction' stamp: 'FA 11/5/2023 17:05:22'!
expiredCreditCard
	|expirationDate|
	"The date is relative with the objetive that this test wont fail if the client changes the computer date"
	expirationDate := GregorianMonthOfYear current previous.
	
	^(CreditCard of: 'Fran' withNumber: '0123456789123456' expiring: expirationDate).
! !

!CashierTest methodsFor: 'testing abstraction' stamp: 'FA 11/6/2023 14:07:03'!
shoppingCartWithMultipleItems
	|cart |

	cart := ShoppingCart forCatalog: self catalogWith2Items.
	cart addBook: 'book1' inAmount: 1.
	cart addBook: 'book2' inAmount: 2.
	
	^cart.! !

!CashierTest methodsFor: 'testing abstraction' stamp: 'FA 11/6/2023 14:07:26'!
shoppingCartWithOneItem
	|cart|
	
	cart := ShoppingCart forCatalog: self catalogWith2Items.
	cart addBook: 'book1' inAmount: 1.
	
	^cart.! !

!CashierTest methodsFor: 'testing abstraction' stamp: 'FA 11/5/2023 17:04:47'!
validCreditCard
	|expirationDate |
	"The date is relative with the objetive that this test wont fail in the future"
	expirationDate := GregorianMonthOfYear current next.

	^(CreditCard of: 'Fran' withNumber: '0123456789123456' expiring: expirationDate).
! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 15:54:44'!
test01CannotCreateCreditCardWithEmptyOwner		
	self should: [CreditCard of: '' withNumber: '0123456789123456' expiring: self october2022]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: CreditCard invalidOwnerErrorDescription].
! !

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 15:54:58'!
test02CannotCreateCreditCardWithOwnerContainingCharactersThatAreNotLetters
	|nameWithNonLetters|
	 
	nameWithNonLetters := 'Fran200'.

	self should: [CreditCard of: nameWithNonLetters withNumber: '0123456789123456' expiring: self october2022]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: CreditCard invalidOwnerErrorDescription].
! !

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 15:55:06'!
test03CannotCreateCreditCardWithCreditCardNumberDifferentThan16	

	self should: [CreditCard of: 'Fran' withNumber: '1240124' expiring: self october2022]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: CreditCard invalidCreditCardNumberErrorDescription].
! !

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 15:55:18'!
test04CannotCreateCreditCardWithCreditCardNumberDifferentThan16	

	self should: [CreditCard of: 'Fran' withNumber: '1240124' expiring: self october2022]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: CreditCard invalidCreditCardNumberErrorDescription].
! !

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 15:55:27'!
test05CannotCreateCreditCardWithCreditCardNumberContainingCharactersThatAreNotNumbers

	self should: [CreditCard of: 'Fran' withNumber: '012345678912345a' expiring: self october2022]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: CreditCard invalidCreditCardNumberErrorDescription].
! !

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 16:17:59'!
test06ValidCardIsExpiredIfCurrentMonthIsAfterThanExpirationDate
	|creditCard|
	
	creditCard :=  (CreditCard of: 'Fran' withNumber: '0123456789123456' expiring: self october2022).
	
	self assert: (creditCard isExpiredAt: self december2022).! !

!CreditCardTest methodsFor: 'tests' stamp: 'FA 11/5/2023 16:01:46'!
test07ValidCardIsNotExpiredIfCurrentMonthIsBeforeExpirationDate
	|creditCard |
	
	creditCard :=  (CreditCard of: 'Fran' withNumber: '0123456789123456' expiring: self october2022).
	
	self deny: (creditCard isExpiredAt: self september2022).! !


!CreditCardTest methodsFor: 'testing abstactions' stamp: 'FA 11/5/2023 17:05:52'!
december2022
	^GregorianMonthOfYear yearNumber: 2022 month: December.
! !

!CreditCardTest methodsFor: 'testing abstactions' stamp: 'FA 11/5/2023 17:06:13'!
october2022
	^GregorianMonthOfYear yearNumber: 2022 month: October.
! !

!CreditCardTest methodsFor: 'testing abstactions' stamp: 'FA 11/5/2023 17:06:22'!
september2022
	^GregorianMonthOfYear yearNumber: 2022 month: September.
! !


!classDefinition: #ShoppingCartTest category: 'TusLibros'!
TestCase subclass: #ShoppingCartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ShoppingCartTest methodsFor: 'tests' stamp: 'FAA 10/30/2023 21:39:24'!
test01CreateCartStartsEmpty
	
	self assert: (ShoppingCart forCatalog: (OrderedCollection with: 1 with: 2)) isEmpty! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 21:27:22'!
test02CannotAddToCartElementNotAvailableInEditorial
	|cart wantedISBN |
	
	cart := ShoppingCart forCatalog: (Dictionary with: 'book2'->1).
	
	wantedISBN := 'book1'.
		
	self should: [cart addBook: wantedISBN inAmount: 1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: ShoppingCart signalBookNotInEditorial.
							self assert: cart isEmpty.
				].! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 21:27:45'!
test03CanAddToCartElementAvailableInEditorial
	|cart wantedISBN|
	
	cart := ShoppingCart forCatalog: (Dictionary with: 'book1'->10).
	
	wantedISBN := 'book1'.
		
	cart addBook: wantedISBN inAmount: 1.
	
	self assert: (cart hasBook: wantedISBN inAmount: 1).! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 21:27:59'!
test04CannotAddToCartANonPositiveAmountOfBooks
	|cart wantedISBN |
	
	cart := ShoppingCart forCatalog: (Dictionary with: 'book1'->10).
	
	wantedISBN := 'book1'.	
	
	self should: [cart addBook: wantedISBN inAmount: 0]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: ShoppingCart signalNonPositiveAmountRequested.
							self assert: cart isEmpty.
				].! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 21:28:06'!
test05AddingToCartAnElementThatWasAlreadyInCartIncreasesAmountInCart
	|cart wantedISBN |
	
	cart := ShoppingCart forCatalog: (Dictionary with: 'book1'->10).
	
	wantedISBN := 'book1'.
		
	cart addBook: wantedISBN inAmount: 1.
	cart addBook: wantedISBN inAmount: 2.
	
	self assert: (cart hasBook: wantedISBN inAmount: 3).! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 21:29:19'!
test06CanAddMultipleBooks
	|cart|
	
	cart := ShoppingCart forCatalog: (Dictionary with: 'book1'->10 with: 'book2'->20).
			
	cart addBook: 'book1' inAmount: 3.
	cart addBook: 'book2' inAmount: 2.
	
	self assert: (cart hasBook: 'book1' inAmount: 3).
	self assert: (cart hasBook: 'book2' inAmount: 2).
	! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'catalog shoppingCart creditCard merchantProcessor hasSold'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'testing' stamp: 'FA 11/5/2023 18:10:48'!
hasSold
	^hasSold! !


!Cashier methodsFor: 'checkOut' stamp: 'FA 11/6/2023 14:16:46'!
assertCreditCardIsNotExpired

	^ (creditCard isExpiredAt: GregorianMonthOfYear current) ifTrue: [self error: self class expiredCardErrorDescription]! !

!Cashier methodsFor: 'checkOut' stamp: 'FA 11/6/2023 14:17:07'!
checkOut 
	|transactionID|
		
	self assertShoppingCartIsNotEmpty.
	self assertCreditCardIsNotExpired.
	
	transactionID := merchantProcessor debit: shoppingCart totalToPay from: creditCard.
	
	hasSold := true.
	
	^transactionID.! !

!Cashier methodsFor: 'checkOut' stamp: 'FA 11/2/2023 19:38:53'!
makeTicketFor: aDIctionaryOfItemsAndAmounts
	|ticket total currentLine currentItemPriceForAmount|

	total := 0.
	ticket := OrderedCollection new.
	currentLine := String new.
	
	aDIctionaryOfItemsAndAmounts keysAndValuesDo: [:anItem :anAmount | 
		currentItemPriceForAmount := (catalog at: anItem) * anAmount.
		total := total + currentItemPriceForAmount.
		currentLine := anAmount asString, ' ',  anItem asString, ' for ', currentItemPriceForAmount asString.
		ticket addFirst: currentLine.
	].

	ticket add: 'total = ', total asString.
	
	^ticket.! !


!Cashier methodsFor: 'initialization' stamp: 'FA 11/5/2023 18:09:14'!
initializeFor: aShoppingCart withCreditCard: aClientsCreditCard using: aMerchantProcessor
	shoppingCart := aShoppingCart.
	creditCard := aClientsCreditCard.
	merchantProcessor :=aMerchantProcessor.
	hasSold := false.! !


!Cashier methodsFor: 'assertions' stamp: 'FA 11/6/2023 14:16:19'!
assertShoppingCartIsNotEmpty
	(shoppingCart isEmpty) ifTrue: [self error: self class emptyCartErrorDescription]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'error description' stamp: 'FA 11/2/2023 18:46:52'!
emptyCartErrorDescription
	^'Cart cannot be empty'! !

!Cashier class methodsFor: 'error description' stamp: 'FA 11/2/2023 19:54:38'!
expiredCardErrorDescription
	^'Credit Card is expired'.! !


!Cashier class methodsFor: 'instance creation' stamp: 'FA 11/5/2023 17:26:30'!
for: shoppingCart withCreditCard: aCreditCard using: aMerchantProcessor
	^self new initializeFor: shoppingCart withCreditCard: aCreditCard using: aMerchantProcessor.! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expirationDate creditCardNumber owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'FA 11/5/2023 15:47:08'!
initializeForOwner: anOwner withNumber: aCreditCardNumber expiring: anExpirationDate
	owner := anOwner.
	creditCardNumber := aCreditCardNumber.
	expirationDate := anExpirationDate! !


!CreditCard methodsFor: 'expiration' stamp: 'FA 11/5/2023 15:51:59'!
isExpiredAt: aMonthOfYear
	^aMonthOfYear > expirationDate.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'FA 11/5/2023 15:46:37'!
of: anOwner withNumber: aCreditCardNumber expiring: anExpirationDate
	self assertIsValidOwner: anOwner.
	
	self assertIsAValidCreditCardNumber: aCreditCardNumber.
	
	
	
	
	^self new initializeForOwner: anOwner withNumber: aCreditCardNumber expiring: anExpirationDate! !


!CreditCard class methodsFor: 'error description' stamp: 'FA 11/5/2023 15:11:37'!
invalidCreditCardNumberErrorDescription
	^'Credit Card number must have 16 characters, all numbers'! !

!CreditCard class methodsFor: 'error description' stamp: 'FA 11/5/2023 14:55:13'!
invalidOwnerErrorDescription
	^'Owner cannot be empty and must have less than 30 characters'! !


!CreditCard class methodsFor: 'assertions' stamp: 'FA 11/6/2023 14:08:58'!
allCharactersAreLetters: anOwner
	^anOwner anySatisfy: [:aCharacter | aCharacter isLetter not]. ! !

!CreditCard class methodsFor: 'assertions' stamp: 'FA 11/6/2023 14:14:57'!
allDigitsAreNumber: aCreditCardNumber
	
	^aCreditCardNumber allSatisfy: [:aCharacter | aCharacter isDigit].! !

!CreditCard class methodsFor: 'assertions' stamp: 'FA 11/6/2023 14:14:57'!
assertIsAValidCreditCardNumber: aCreditCardNumber.
	((aCreditCardNumber size = 16) and: [self allDigitsAreNumber: aCreditCardNumber ]) ifFalse: [self error: self invalidCreditCardNumberErrorDescription].! !

!CreditCard class methodsFor: 'assertions' stamp: 'FA 11/6/2023 14:08:58'!
assertIsValidOwner: anOwner
	(anOwner isEmpty or: [(anOwner size > 30)] or: [self allCharactersAreLetters: anOwner]) ifTrue: [self error: self invalidOwnerErrorDescription]. ! !


!classDefinition: #MerchantProcessor category: 'TusLibros'!
Object subclass: #MerchantProcessor
	instanceVariableNames: 'transactionMessage closureToExecute currentTransactionID lastPurchaseAmount lastPurchase'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessor methodsFor: 'initialization' stamp: 'FA 11/5/2023 18:33:57'!
initializeForAction:	 anInitializationClosure
	currentTransactionID := 1.
	lastPurchaseAmount := 0. 
	closureToExecute := anInitializationClosure! !


!MerchantProcessor methodsFor: 'debiting' stamp: 'FA 11/6/2023 14:03:46'!
debit: anAmount from: aCreditCard 
	|transactionID|
	transactionID := closureToExecute value: currentTransactionID. 
	
	currentTransactionID := transactionID+1.
	lastPurchase := anAmount.
	
	^transactionID.! !


!MerchantProcessor methodsFor: 'testing' stamp: 'FA 11/5/2023 18:44:43'!
lastPurchaseWasForAmount: aTransactionAmount
	^lastPurchase = aTransactionAmount.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessor class' category: 'TusLibros'!
MerchantProcessor class
	instanceVariableNames: ''!

!MerchantProcessor class methodsFor: 'instance creation' stamp: 'FA 11/5/2023 17:39:18'!
withAction: anInitializationClosure
	^self new initializeForAction: anInitializationClosure.! !


!MerchantProcessor class methodsFor: 'error description' stamp: 'FA 11/5/2023 17:51:59'!
insuficientDebitErrorDescription
	'There is no enough money to pay for the requested transaction'! !

!MerchantProcessor class methodsFor: 'error description' stamp: 'FA 11/6/2023 14:04:11'!
simulatingInsuficientCreditDetection
	^self withAction: [:ID | self error: MerchantProcessor insuficientDebitErrorDescription].! !

!MerchantProcessor class methodsFor: 'error description' stamp: 'FA 11/6/2023 14:08:28'!
simulatingStolenCardDetection
	^self withAction: [:ID | self error: MerchantProcessor stolenCardErrorDescription].! !

!MerchantProcessor class methodsFor: 'error description' stamp: 'FA 11/6/2023 14:04:16'!
simulatingValidTransactions
	^ self withAction: [:anID| anID]! !

!MerchantProcessor class methodsFor: 'error description' stamp: 'FA 11/5/2023 17:35:31'!
stolenCardErrorDescription
	^'Credit card was marked as stolen'! !


!classDefinition: #ShoppingCart category: 'TusLibros'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'books catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ShoppingCart methodsFor: 'testing' stamp: 'FA 11/2/2023 19:34:45'!
hasBook: wantedISBN inAmount: anExpectedAmount 
	
	^(books at: wantedISBN ifAbsent: [^false]) = anExpectedAmount! !

!ShoppingCart methodsFor: 'testing' stamp: 'FA 11/2/2023 10:37:45'!
isEmpty
	^books isEmpty.! !


!ShoppingCart methodsFor: 'assertions' stamp: 'FA 11/2/2023 10:34:08'!
assertAmountRequestedIsPositive: aWantedAmount.
	(aWantedAmount < 1) ifTrue: [self error: self class signalNonPositiveAmountRequested].

! !

!ShoppingCart methodsFor: 'assertions' stamp: 'FA 11/2/2023 21:26:55'!
assertBookIsInEditorialCatalog: wantedISBN
	(catalog includesKey: wantedISBN) ifFalse: [self error: self class signalBookNotInEditorial].
! !


!ShoppingCart methodsFor: 'adding' stamp: 'FA 11/2/2023 19:33:50'!
addBook: wantedISBN inAmount: aWantedAmount
	self assertBookIsInEditorialCatalog: wantedISBN.
	self assertAmountRequestedIsPositive: aWantedAmount.
			
	^books add: wantedISBN->((books at: wantedISBN ifAbsent: [0]) + aWantedAmount).! !


!ShoppingCart methodsFor: 'initialization' stamp: 'FA 11/2/2023 19:32:59'!
initializeForCatalog: anISBNCatalog
	catalog := anISBNCatalog.
	books := Dictionary new.! !


!ShoppingCart methodsFor: 'lisiting' stamp: 'FA 11/2/2023 19:00:52'!
listCart
	^books copy.! !

!ShoppingCart methodsFor: 'lisiting' stamp: 'FA 11/2/2023 21:40:49'!
totalToPay
	|total|

	total := 0.
	
	books keysAndValuesDo: [:anItem :anAmount | total := total + (catalog at: anItem) * anAmount.].
	
	^total.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: 'TusLibros'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'error description' stamp: 'FAA 10/30/2023 19:33:46'!
signalBookNotInEditorial
	^'Book not currently available in our editorial'.! !

!ShoppingCart class methodsFor: 'error description' stamp: 'FAA 10/30/2023 20:08:02'!
signalNonPositiveAmountRequested
	^'You must request a positive amount of books'! !


!ShoppingCart class methodsFor: 'instance creation' stamp: 'FAA 10/30/2023 21:38:18'!
forCatalog: anISBNCatalog
	 ^self new initializeForCatalog: anISBNCatalog.! !
