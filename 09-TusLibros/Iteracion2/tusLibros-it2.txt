¿Por donde seguir?

Hay gente que, en la practica, seguiria por la Interfaz, mas que nada mostrarle al cliente algo funcionado.
En metodologias agiles, tipo "Scrum", en las que iteraciones de 1 semana y al final de la semana se muestra al usuario, son muy representativas de esta filosofia "anti-waterfall",
que busca darle "feedaback inmediato" al cliente (lo mismo que buscamos nosotros, tambien lo quieren todos)

-----------------------------------------------------------------------------------------------------------------------------
CHECK-OUT

El checkOut, claramente no es responsabilidad del carrito. Un carrito no hace un checkout, eso lo hace un CAJERO. Bajo la metodlogia de ZOMBIES, hay 3 test obvios:

0 productos  (deberia levantar excepciones)
1 producto
mas de un producto.

Ojo porque el test de 0 productos ademas de la excepcion hay que testear ESTADO. En este caso el estado seria confirmar que NO se hizo el checkOut.
(Cuando no sabemos que testear ademas de la exepcion, es que nos esta faltando algo).

Si no se como decir que no se efectuo la compro, a lo mejor conviene instanciar al cajero con el carrito y la tarjeta como colaborador.
Me hace esos test mas faciles porque ni siquiera se va a poder crear el cajero (va a fallar en la creacion).
Ademas si no iba a romper el acoplamiento, de la tarjeta de credito para pedirle la fecha de expiracion.

no se pueda hacer checkout con una tarjeta vencida 

La responsabilidad deberia ser de la tarjeta (por heursitica de antropomorfismo de objetos), para evitar codigo repetido si ese Objeto CreditCard, se reutiliza 
en otras aplicaciones.
Ojo porque la tarjeta no deberia tener la responsabilidad de COBRAR. Esto porque la tarjeta es solo un identificador de la cuenta, no la cuenta bancaria en si.
--------------------------------------------------------------------------------------------------------------------
CREDIT CARD

Hay que modelar tarjeta de credito (o buscar algo). Tiene que correr ademas en cualquier fecha que sea el dia de HOY. 
Ademas de los colaboradores, el modelado del tarjeta de credito puede incluir los chequeos de creacion.
Esos chequeos hacen que la clase no sea anemica.

-Cantidad de digitos
-Que algun digito sea letra
-Que tenga un owner (no puede ser string vacio)

Ojo porque esto implica que tenemos que tener una suite de tests de CreditCart.

Los test de expiracion no tienen nunca que pasar de validos a invalidos involuntariamente (esto porque un test que hoy pasa porque la tarjeta no expiro, en unos años va a fallar).
Para esto tengo opciones:

-Hacer las fechas relativas al dia de hoy
-Hacer las fechas absoulutas (a alguna fecha hardcodeado de referencia)

El mensaje deberia ser "isExpiredAt: aMonthAndYear" que me permita hacerlo con fechas absolutas, y no solo contrastandolo contra Today.

Para no acoplar tambien el cajero a "Date", le tendria que pasar tambien el dia en el que esta como colaborador.

---------------------------------------------------------------------------------------------------------------------------------------
El carrito puede ser el responsable de calcular el total (de hecho muchos carritos de compra permiten ver el total si ver el checkout).
Y puedo tener la lista de precios en un solo lugar.

El ticket no hace  falta AHORA. Pero para el historial lo vamos a necesitar.

------------------------------------
COBRO

De esto esta encargado el MerchantProcessor (MP). Hay que mandarle un mensaje HTTP a ese sistema externo. El problema de esto es que, en el desarrolo, esas request
me estan costando plata al pedo. Ademas depender (estar acoplado a) al MP para los test es PELIGROSO.
Los test pueden fallar, cuando en realidad falla el MP. O a lo mejor el MP esta acoplado a una base de datos. Si algo le paso a la base de datos, el test falla.
No tener test controlados en mi ambiente, genera test ERRATICOS (a veces pasa a veces no).

Luego queremos simular al MP.

La interfaz REST simplemente traduce de un idioma a otro. En esta interfaz particular , la parte interna habla OBJETOS, y la externa habla HTTP.
Inicialmente, voy a simular la cara interna, pues todavia estoy trabajando con el cajero, no con la interfaz

test01 - No puedo vender si la cuenta denuncio la tarjeta como robada
test02 - No puedo vender si no tengo credito suficiente en la cuenta
test03 - Procesar pago para una cuenta valda.

La interfaz interna la defino yo, y por ende tambien como la simulo (la del enunciado es externa, con HTTP)

aVotar!   (password de la solucion de it.1).

------------------------------------------------------------------------------------------------------------------------------

TEST CASES 

Para evitar codigo repetido (poder usar las e), a veces es mejor un solo TestCase y despues categorizar los test de cada clase.

--------------------------------------------------------------------------

FEEDBACK

¿Que es que el codigo ande bien? Cuando el cliente esta conforme.

Continous delievery vs Feature Branching.

INCREMENTALISMO

Agregar de a pasitos, me facilita saber que fallo (lo que acabe de agregar).
Si hago un merge de 100 cosas, no se donde rompi

Herrmientas para incrementalismo:

-TDD
-Refactorings (solo hago UN refactoring a la vez)
-Versioning (En la practica GIT / GIT BISSECT).
-Test automatizados (tienen que andar rapido sino no hay chiste)
-Microservicios

Proyecto estrella del incrementalismo: Apollo 11 (los anteriores se rompieron todos, se fueron armando de manera incremental).
Cada iteracion fueron agregando cosas nuevas.
