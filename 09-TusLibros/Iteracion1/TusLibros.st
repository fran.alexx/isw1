!classDefinition: #ShoppingCartTest category: 'TusLibros'!
TestCase subclass: #ShoppingCartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ShoppingCartTest methodsFor: 'tests' stamp: 'FAA 10/30/2023 21:39:24'!
test01CreateCartStartsEmpty
	
	self assert: (ShoppingCart forCatalog: (OrderedCollection with: 1 with: 2)) isEmpty! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 10:31:52'!
test02CannotAddToCartElementNotAvailableInEditorial
	|cart wantedISBN |
	
	cart := ShoppingCart forCatalog: (OrderedCollection with: 2).
	
	wantedISBN := 1.
		
	self should: [cart addBook: wantedISBN inAmount: 1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: ShoppingCart signalBookNotInEditorial.
							self assert: cart isEmpty.
				].! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 10:31:53'!
test03CanAddToCartElementAvailableInEditorial
	|cart wantedISBN|
	
	cart := ShoppingCart forCatalog: (OrderedCollection with: 1).
	
	wantedISBN := 1.
		
	cart addBook: wantedISBN inAmount: 1.
	
	self assert: (cart hasBook: wantedISBN inAmount: 1).! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 10:31:53'!
test04CannotAddToCartANonPositiveAmountOfBooks
	|cart wantedISBN |
	
	cart := ShoppingCart forCatalog: (OrderedCollection with: 1).
	
	wantedISBN := 1.	
	
	self should: [cart addBook: wantedISBN inAmount: 0]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anErrorText | self assert: anErrorText messageText equals: ShoppingCart signalNonPositiveAmountRequested.
							self assert: cart isEmpty.
				].! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 10:31:53'!
test05AddingToCartAnElementThatWasAlreadyInCartIncreasesAmountInCart
	|cart wantedISBN |
	
	cart := ShoppingCart forCatalog: (OrderedCollection with: 1).
	
	wantedISBN := 1.
		
	cart addBook: wantedISBN inAmount: 1.
	cart addBook: wantedISBN inAmount: 2.
	
	self assert: (cart hasBook: wantedISBN inAmount: 3).! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FA 11/2/2023 10:31:52'!
test06CanAddMultipleBooks
	|cart ISBNsInStock|
	
	cart := ShoppingCart forCatalog: (OrderedCollection with: 1 with: 2).
		
	ISBNsInStock := OrderedCollection with: 1 with: 2.
	
	cart addBook: 1 inAmount: 3.
	cart addBook: 2 inAmount: 2.
	
	self assert: (cart hasBook: 1 inAmount: 3).
	self assert: (cart hasBook: 2 inAmount: 2).
	! !


!classDefinition: #ShoppingCart category: 'TusLibros'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'books catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ShoppingCart methodsFor: 'testing' stamp: 'FA 11/2/2023 10:37:45'!
hasBook: wantedISBN inAmount: anExpectedAmount 
	
	^(books at: wantedISBN ifAbsent: [^false]) = anExpectedAmount! !

!ShoppingCart methodsFor: 'testing' stamp: 'FA 11/2/2023 10:37:45'!
isEmpty
	^books isEmpty.! !


!ShoppingCart methodsFor: 'assertions' stamp: 'FA 11/2/2023 10:34:08'!
assertAmountRequestedIsPositive: aWantedAmount.
	(aWantedAmount < 1) ifTrue: [self error: self class signalNonPositiveAmountRequested].

! !

!ShoppingCart methodsFor: 'assertions' stamp: 'FA 11/2/2023 10:32:58'!
assertBookIsInEditorialCatalog: wantedISBN
	(catalog includes: wantedISBN) ifFalse: [self error: self class signalBookNotInEditorial].
! !


!ShoppingCart methodsFor: 'adding' stamp: 'FA 11/2/2023 10:43:21'!
addBook: wantedISBN inAmount: aWantedAmount
	self assertBookIsInEditorialCatalog: wantedISBN.
	self assertAmountRequestedIsPositive: aWantedAmount.
			
	^books add: wantedISBN->((books at: wantedISBN ifAbsent: [0]) + aWantedAmount) .! !


!ShoppingCart methodsFor: 'initialization' stamp: 'FA 11/2/2023 10:37:45'!
initializeForCatalog: anISBNCatalog
	catalog := anISBNCatalog.
	books := Dictionary new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: 'TusLibros'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'error description' stamp: 'FAA 10/30/2023 19:33:46'!
signalBookNotInEditorial
	^'Book not currently available in our editorial'.! !

!ShoppingCart class methodsFor: 'error description' stamp: 'FAA 10/30/2023 20:08:02'!
signalNonPositiveAmountRequested
	^'You must request a positive amount of books'! !


!ShoppingCart class methodsFor: 'instance creation' stamp: 'FAA 10/30/2023 21:38:18'!
forCatalog: anISBNCatalog
	 ^self new initializeForCatalog: anISBNCatalog.! !
