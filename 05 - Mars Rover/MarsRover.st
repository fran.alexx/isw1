!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test01RoverStartsAtOrderedPositionAndDirection

	|marsRover |
	
	marsRover := MarsRover at: 0@0 pointingTo:#North.
	
	self assert: (marsRover isAt: 0@0 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test02EmptyCommandsDoesNotMoveRover

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := ''.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@0 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:51'!
test03OneForwardCommandAdvancesYposition

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'f'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@1 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test04MultipleForwardCommandsAdvancesYposition

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'ff'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@2 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test05OneBackwardCommandDecreasesYposition

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'b'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@-1 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test06OneLeftCommandChangesDirection

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'l'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@0 poiting: West).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test07MultipleLeftCommandChangesDirectionCorrectly

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'll'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@0 poiting: South).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:51'!
test08OneRightCommandChangesDirectionCorrectly

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'r'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@0 poiting: East).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test09TurnRightAndThenForwardMultipleTimes

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'rfrfrfrf'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@0 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test10TurnLeftAndThenBackwardMultipleTimes

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'lblblblb'.
	
	marsRover processCommandPackage: commands.
	
	self assert: (marsRover isAt: 0@0 poiting: North).! !

!MarsRoverTest methodsFor: 'tests' stamp: 'F 10/8/2023 18:24:52'!
test11InvalidCommandInTheMiddle

	|marsRover commands|
	
	marsRover := MarsRover at: 0@0 pointingTo: #North.
	commands := 'frfr@ffff'.
		
	self should: [marsRover processCommandPackage: commands] 
	raise: Error 
	description: marsRover class invalidCommandErrorDescription.
	
	self assert: (marsRover isAt: 1@1 poiting: South).

	! !


!classDefinition: #CardinalDirection category: 'MarsRover'!
Object subclass: #CardinalDirection
	instanceVariableNames: 'rover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!CardinalDirection methodsFor: 'initialization' stamp: 'F 10/8/2023 16:20:59'!
for: aRover
	rover := aRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CardinalDirection class' category: 'MarsRover'!
CardinalDirection class
	instanceVariableNames: ''!

!CardinalDirection class methodsFor: 'instance creation' stamp: 'F 10/8/2023 16:21:58'!
ofRover: aMarsRover pointing: currentDirection
 	|correctSubclass|
	correctSubclass := CardinalDirection allSubclasses detect: [:cardinalSubclass | cardinalSubclass canHandle: currentDirection].
	
	^correctSubclass new for: aMarsRover.! !


!classDefinition: #East category: 'MarsRover'!
CardinalDirection subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!East methodsFor: 'rotation' stamp: 'F 10/8/2023 16:28:14'!
turn90DegreesLeft
	rover changeDirection: #North. ! !

!East methodsFor: 'rotation' stamp: 'F 10/8/2023 16:28:21'!
turn90DegreesRight
	rover changeDirection: #South ! !


!East methodsFor: 'movement' stamp: 'F 10/8/2023 17:06:13'!
moveBackwardInDirection
	rover moveBackwardFromEast! !

!East methodsFor: 'movement' stamp: 'F 10/8/2023 17:01:50'!
moveForwardInDirection
	rover moveForwardFromEast! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'East class' category: 'MarsRover'!
East class
	instanceVariableNames: ''!

!East class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:16:37'!
canHandle: aCardinal
	^aCardinal = #East! !


!classDefinition: #North category: 'MarsRover'!
CardinalDirection subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!North methodsFor: 'rotation' stamp: 'F 10/8/2023 16:28:37'!
turn90DegreesLeft
	rover changeDirection: #West. 
	! !

!North methodsFor: 'rotation' stamp: 'F 10/8/2023 16:28:31'!
turn90DegreesRight
	rover changeDirection: #East. 
	! !


!North methodsFor: 'movement' stamp: 'F 10/8/2023 17:06:29'!
moveBackwardInDirection
	rover moveBackwardFromNorth.! !

!North methodsFor: 'movement' stamp: 'F 10/8/2023 17:02:16'!
moveForwardInDirection
	rover moveForwardFromNorth! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'North class' category: 'MarsRover'!
North class
	instanceVariableNames: ''!

!North class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:17:08'!
canHandle: aCardinal
	^aCardinal = #North! !


!classDefinition: #South category: 'MarsRover'!
CardinalDirection subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!South methodsFor: 'rotation' stamp: 'F 10/8/2023 16:28:49'!
turn90DegreesLeft
	rover changeDirection: #East. ! !

!South methodsFor: 'rotation' stamp: 'F 10/8/2023 16:29:36'!
turn90DegreesRight
	rover changeDirection: #West. 
	! !


!South methodsFor: 'movement' stamp: 'F 10/8/2023 17:06:42'!
moveBackwardInDirection
	rover moveBackwardFromSouth.! !

!South methodsFor: 'movement' stamp: 'F 10/8/2023 17:00:25'!
moveForwardInDirection
	rover moveForwardFromSouth! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'South class' category: 'MarsRover'!
South class
	instanceVariableNames: ''!

!South class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:17:17'!
canHandle: aCardinal
	^aCardinal = #South! !


!classDefinition: #West category: 'MarsRover'!
CardinalDirection subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!West methodsFor: 'rotation' stamp: 'F 10/8/2023 16:28:58'!
turn90DegreesLeft
	rover changeDirection: #South. ! !

!West methodsFor: 'rotation' stamp: 'F 10/8/2023 16:29:08'!
turn90DegreesRight
	rover changeDirection: #North. 
	! !


!West methodsFor: 'movement' stamp: 'F 10/8/2023 17:07:00'!
moveBackwardInDirection
	rover moveBackwardFromWest.! !

!West methodsFor: 'movement' stamp: 'F 10/8/2023 17:02:22'!
moveForwardInDirection
	rover moveForwardFromWest! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'West class' category: 'MarsRover'!
West class
	instanceVariableNames: ''!

!West class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:17:25'!
canHandle: aCardinal
	^aCardinal = #West! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'direction position'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'initialization' stamp: 'F 10/8/2023 15:43:36'!
instanceAt: aStartingPoint pointingTo: aCardinalDirection.
	position := aStartingPoint.
	direction :=  CardinalDirection ofRover: self pointing: aCardinalDirection.! !


!MarsRover methodsFor: 'testing' stamp: 'F 10/8/2023 15:50:06'!
isAt: expectedPosition poiting: expectedCardinalDirection 
	^ (position = expectedPosition) and: [direction class= expectedCardinalDirection].! !


!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 16:28:06'!
changeDirection: aCardinalDirection
	direction := (CardinalDirection ofRover: self pointing: aCardinalDirection).! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 17:21:00'!
invalidCommand
	self error: self class invalidCommandErrorDescription! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 17:05:52'!
moveBackward
	direction moveBackwardInDirection.
	
! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 17:04:12'!
moveForward
	direction moveForwardInDirection
	
! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 18:23:32'!
processCommand: aCommand  
	
	(MarsRoverCommand command: aCommand for: self) execute.
			! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 18:22:42'!
processCommandPackage: aStringOfCommands  
	
	aStringOfCommands do: [:aCommand | self 	processCommand: aCommand].
		! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 18:09:30'!
rotate90DegreesLeft
	direction turn90DegreesLeft.! !

!MarsRover methodsFor: 'commands' stamp: 'F 10/8/2023 18:09:53'!
rotate90DegreesRight
	direction turn90DegreesRight.! !


!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:11:23'!
moveBackwardFromEast
	position := position + (-1@0).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:11:51'!
moveBackwardFromNorth
	position := position + (0@-1).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:12:21'!
moveBackwardFromSouth
	position := position + (0@1).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:12:31'!
moveBackwardFromWest
	position := position + (1@0).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 16:59:57'!
moveForwardFromEast
	position := position + (1@0).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:00:08'!
moveForwardFromNorth
	position := position + (0@1).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:00:24'!
moveForwardFromSouth
	position := position + (0@-1).! !

!MarsRover methodsFor: 'commans - private' stamp: 'F 10/8/2023 17:00:48'!
moveForwardFromWest
	position := position + (-1@0).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'error descriptions' stamp: 'F 10/8/2023 18:18:17'!
invalidCommandErrorDescription
	^'The only available commands are: "f", "b", "l" and "r"'! !


!MarsRover class methodsFor: 'instance creation' stamp: 'F 10/8/2023 18:24:51'!
at: aStartingPoint pointingTo: aCardinal 
	^MarsRover new instanceAt: aStartingPoint pointingTo:aCardinal.! !


!classDefinition: #MarsRoverCommand category: 'MarsRover'!
Object subclass: #MarsRoverCommand
	instanceVariableNames: 'rover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverCommand methodsFor: 'initialization' stamp: 'F 10/8/2023 16:39:47'!
for: aRover
	rover := aRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommand class' category: 'MarsRover'!
MarsRoverCommand class
	instanceVariableNames: ''!

!MarsRoverCommand class methodsFor: 'instance creation' stamp: 'F 10/8/2023 16:39:21'!
command: aCommand for: aRover
	|correctSubclass|
	correctSubclass := MarsRoverCommand allSubclasses detect: [:commandSubclass | commandSubclass canHandle: aCommand].
	
	^correctSubclass new for: aRover.! !


!classDefinition: #BackwardCommand category: 'MarsRover'!
MarsRoverCommand subclass: #BackwardCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!BackwardCommand methodsFor: 'action' stamp: 'F 10/8/2023 16:42:10'!
execute
	rover moveBackward! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'BackwardCommand class' category: 'MarsRover'!
BackwardCommand class
	instanceVariableNames: ''!

!BackwardCommand class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:35:14'!
canHandle: aCommand
	^aCommand = $b.! !


!classDefinition: #ForwardCommand category: 'MarsRover'!
MarsRoverCommand subclass: #ForwardCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!ForwardCommand methodsFor: 'action' stamp: 'F 10/8/2023 16:42:17'!
execute
	rover moveForward! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ForwardCommand class' category: 'MarsRover'!
ForwardCommand class
	instanceVariableNames: ''!

!ForwardCommand class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:33:46'!
canHandle: aCommand
	^aCommand = $f.! !


!classDefinition: #InvalidCommand category: 'MarsRover'!
MarsRoverCommand subclass: #InvalidCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!InvalidCommand methodsFor: 'action' stamp: 'F 10/8/2023 17:20:28'!
execute
	rover invalidCommand! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'InvalidCommand class' category: 'MarsRover'!
InvalidCommand class
	instanceVariableNames: ''!

!InvalidCommand class methodsFor: 'state selection' stamp: 'F 10/8/2023 17:19:33'!
canHandle: aCommand
	^(aCommand = $b or: [aCommand = $f] or: [aCommand = $r] or: [aCommand = $l]) not.! !


!classDefinition: #RotateLeftCommand category: 'MarsRover'!
MarsRoverCommand subclass: #RotateLeftCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RotateLeftCommand methodsFor: 'action' stamp: 'F 10/8/2023 18:09:30'!
execute
	rover rotate90DegreesLeft! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RotateLeftCommand class' category: 'MarsRover'!
RotateLeftCommand class
	instanceVariableNames: ''!

!RotateLeftCommand class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:34:55'!
canHandle: aCommand
	^aCommand = $l.! !


!classDefinition: #RotateRightCommand category: 'MarsRover'!
MarsRoverCommand subclass: #RotateRightCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RotateRightCommand methodsFor: 'action' stamp: 'F 10/8/2023 18:09:53'!
execute
	rover rotate90DegreesRight! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RotateRightCommand class' category: 'MarsRover'!
RotateRightCommand class
	instanceVariableNames: ''!

!RotateRightCommand class methodsFor: 'state selection' stamp: 'F 10/8/2023 16:35:06'!
canHandle: aCommand
	^aCommand = $r.! !
