!classDefinition: #PersistentSet category: 'CustomerImporter'!
Set subclass: #PersistentSet
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!PersistentSet methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:25:31'!
initializeOn: aSession from: aNonPersistentSet

	session := aSession.
	self addAll: aNonPersistentSet ! !


!PersistentSet methodsFor: 'adding' stamp: 'HAW 11/14/2023 08:23:40'!
add: newObject

	super add: newObject.
	session persist: newObject.
	
	^newObject! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PersistentSet class' category: 'CustomerImporter'!
PersistentSet class
	instanceVariableNames: ''!

!PersistentSet class methodsFor: 'instance creation' stamp: 'HAW 11/14/2023 08:24:32'!
on: aSession

	^self on: aSession from: #()! !

!PersistentSet class methodsFor: 'instance creation' stamp: 'HAW 11/14/2023 08:25:00'!
on: aSession from: aNonPersistentSet

	^self new initializeOn: aSession from: aNonPersistentSet
! !


!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'HAW 5/22/2022 18:17:51'!
test01Import

	CustomerImporter valueFrom: self validImportData into: session..

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'f 11/20/2023 14:33:55'!
test02CustomerWithLessThan5ElementsProducesError


	self should:[CustomerImporter valueFrom: (ReadStream on:
'C,Pepe,Sanchez,22333444') into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter invalidSizeOfCustomerErrorDescription.
		 self assertDatabaseHasNoCustomers]

	! !

!ImportTest methodsFor: 'tests' stamp: 'f 11/20/2023 14:33:52'!
test03CustomerWithMoreThan5ElementsProducesError



	self should:[CustomerImporter valueFrom: (ReadStream on:
'C,Pepe,Sanchez,D,22333,444') into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter invalidSizeOfCustomerErrorDescription.
		self assertDatabaseHasNoCustomers]

	! !

!ImportTest methodsFor: 'tests' stamp: 'FA 11/22/2023 10:36:25'!
test04AddressWithLessThan6ElementsProducesError

	

	self should:[CustomerImporter valueFrom: (ReadStream on:
self insuficientAddressLengthImport) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter invalidSizeOfAddressErrorDescription.
		self assertPepeSanchezWasImportedCorrecty.
		self assertPepeHasOnly2Addresses ]

	! !

!ImportTest methodsFor: 'tests' stamp: 'FA 11/22/2023 10:36:51'!
test05AddressWithMoreThan6ElementsProducesError

	
	self should:[CustomerImporter valueFrom: (ReadStream on:
self excessAddressLengthImport) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter invalidSizeOfAddressErrorDescription .
		self assertPepeSanchezWasImportedCorrecty.
		self assertPepeHasOnly2Addresses]

	! !

!ImportTest methodsFor: 'tests' stamp: 'f 11/20/2023 15:02:24'!
test06LineNotStartingWithCOrAProducesError

	self should:[CustomerImporter valueFrom: (ReadStream on:
'Ca,Pepe,Sanchez,D,22333444') into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter invalidTypeOfDataErrorDescription.
		 self assertDatabaseHasNoCustomers]

	! !

!ImportTest methodsFor: 'tests' stamp: 'FA 11/22/2023 10:34:50'!
test07ImportIgnoresExtraEmptySpaces
 
CustomerImporter valueFrom: (ReadStream on:
self extraSpacesImport) into: session.


	self assertPepeSanchezWasImportedCorrecty.! !

!ImportTest methodsFor: 'tests' stamp: 'f 11/20/2023 16:49:26'!
test08importAddressWithoutCustomerProducesAnError

	self should:[CustomerImporter valueFrom: (ReadStream on:
'A,San Martin,3322,Olivos,1636,BsAs') into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter addressWithoutCustomerErrorDescription .
		self assertDatabaseHasNoCustomers .]
! !

!ImportTest methodsFor: 'tests' stamp: 'FA 11/22/2023 10:34:04'!
test09ImportingEmptyLineProducesError

	self should:[CustomerImporter valueFrom: (ReadStream on:
self emptyLineImport) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: anError messageText  equals: CustomerImporter emptyLineErrorDescription.
		self assertPepeSanchezWasImportedCorrecty.
		self assertPepeHasOnly2Addresses].

	! !


!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/20/2023 16:59:27'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := session customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/20/2023 16:53:54'!
assertDatabaseHasNoCustomers

	^ self assert: 0 equals: session numberOfCustomers! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/20/2023 16:53:09'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: session numberOfCustomers! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/20/2023 17:09:46'!
assertPepeHasOnly2Addresses
	|aCustomer|
	aCustomer:=(session customerWithIdentificationType: 'D' number: '22333444').
	self assert: aCustomer addresses size = 2! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'f 11/20/2023 16:22:21'!
setUp

	session := CustomerSystem start.
	session beginTransaction.
! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !


!ImportTest methodsFor: 'test data' stamp: 'FA 11/22/2023 10:34:04'!
emptyLineImport

	^ 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires

A,Alem,1122,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'FA 11/22/2023 10:36:51'!
excessAddressLengthImport

	^ 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
A,Alem,11,22,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'FA 11/22/2023 10:34:50'!
extraSpacesImport

	^ 'C,Pepe,        Sanchez,D,22333444
A,San      Martin,3322,Olivos          ,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires'! !

!ImportTest methodsFor: 'test data' stamp: 'FA 11/22/2023 10:36:25'!
insuficientAddressLengthImport

	^ 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
A,Alem,1122,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'HAW 5/22/2022 18:08:08'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream line newCustomer record hasCustomer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'f 11/20/2023 15:44:47'!
initializeFrom: aReadStream into: aSession
	session := aSession.
	readStream := aReadStream.
	hasCustomer:=false.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'f 11/16/2023 19:20:47'!
value

	"
	self importCustomers
	"
	 
	[ self lineIsValid ] whileTrue: [
		self createRecord.
		self importRecord.
	].

	! !


!CustomerImporter methodsFor: 'error handling' stamp: 'f 11/20/2023 15:01:06'!
signalEmptyLineError
	^self error: self class emptyLineErrorDescription. ! !

!CustomerImporter methodsFor: 'error handling' stamp: 'f 11/16/2023 20:05:54'!
signalInvalidAddressSizeError
	^self error: self class invalidSizeOfAddressErrorDescription .! !

!CustomerImporter methodsFor: 'error handling' stamp: 'f 11/16/2023 20:06:08'!
signalInvalidCustomerSizeError
	^self error: self class invalidSizeOfCustomerErrorDescription .! !

!CustomerImporter methodsFor: 'error handling' stamp: 'f 11/16/2023 20:11:32'!
signalInvalidTypeOfDataError
	^self error: self class invalidTypeOfDataErrorDescription .! !


!CustomerImporter methodsFor: 'record' stamp: 'f 11/20/2023 14:28:59'!
normalizeRecordElements
	record:=record collect:[:anElem | anElem withBlanksCondensed].
	
	
	
	! !


!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 14:57:53'!
createRecord
	
	(line withBlanksCondensed size = 0) ifTrue:[ self signalEmptyLineError].
	 record := line findTokens: $, .
	 self normalizeRecordElements.
	^record.! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 15:42:33'!
importAddress

	| addressData newAddress |
			(record size ~= 6)ifTrue:[self signalInvalidAddressSizeError].
			(hasCustomer) ifFalse:[self signalAddressWithoutCustomerError].
			addressData := record.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth.! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 15:39:49'!
importCustomer

		 | customerData |	.
			(record size ~= 5)ifTrue:[self signalInvalidCustomerSizeError].
			customerData := record.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth.
			session persist: newCustomer.
			hasCustomer:=true.! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 14:48:49'!
importRecord

	
	(self isACustomerLine) ifTrue: [^self importCustomer].
	(self isAnAddressLine) ifTrue: [^ self importAddress].
	self signalInvalidTypeOfDataError 
		! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 14:43:54'!
isACustomerLine

	^ (record first = 'C')! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 14:44:05'!
isAnAddressLine

	^ (record first = 'A')! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 14:57:19'!
lineIsValid

	line := readStream nextLine.
	^line notNil! !

!CustomerImporter methodsFor: 'abstractions' stamp: 'f 11/20/2023 15:42:33'!
signalAddressWithoutCustomerError

	^ self error: self class addressWithoutCustomerErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'error handling' stamp: 'f 11/20/2023 15:43:06'!
addressWithoutCustomerErrorDescription
	^'cannot add an address without a customer'! !

!CustomerImporter class methodsFor: 'error handling' stamp: 'f 11/20/2023 14:56:06'!
emptyLineErrorDescription
	^'a line cannot be empty'! !

!CustomerImporter class methodsFor: 'error handling' stamp: 'f 11/16/2023 20:05:54'!
invalidSizeOfAddressErrorDescription
	^'the amount of elements of an address must be 6'! !

!CustomerImporter class methodsFor: 'error handling' stamp: 'f 11/16/2023 20:06:08'!
invalidSizeOfCustomerErrorDescription
	^'the amount of elements of a customer must be 5'! !

!CustomerImporter class methodsFor: 'error handling' stamp: 'f 11/16/2023 20:10:31'!
invalidTypeOfDataErrorDescription
	^'the only valid elements to import are customers(C) or addresses(A)'! !


!CustomerImporter class methodsFor: 'importing' stamp: 'HAW 5/22/2022 18:11:27'!
valueFrom: aReadStream into: aSession

	^(self from: aReadStream into: aSession) value! !


!classDefinition: #CustomerSystem category: 'CustomerImporter'!
Object subclass: #CustomerSystem
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerSystem methodsFor: 'adding' stamp: 'f 11/20/2023 16:35:28'!
persist:anObject
	self subclassResponsibility! !


!CustomerSystem methodsFor: 'transaction/commit' stamp: 'f 11/20/2023 16:30:24'!
beginTransaction
	^self subclassResponsibility! !

!CustomerSystem methodsFor: 'transaction/commit' stamp: 'f 11/20/2023 16:28:29'!
commit
	self subclassResponsibility! !


!CustomerSystem methodsFor: 'start/close' stamp: 'f 11/20/2023 16:28:32'!
close
	self subclassResponsibility! !


!CustomerSystem methodsFor: 'access' stamp: 'FA 11/22/2023 10:44:07'!
customerWithIdentificationType: anIdType number: anIdNumber
	self subclassResponsibility.! !


!CustomerSystem methodsFor: 'testing' stamp: 'f 11/20/2023 16:51:30'!
numberOfCustomers
	self subclassResponsibility! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerSystem class' category: 'CustomerImporter'!
CustomerSystem class
	instanceVariableNames: ''!

!CustomerSystem class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:26:07'!
start
	^Enviroment create
	"^PersistentCustomerSystem start.
	^TransientCustomerSystem start. "! !


!classDefinition: #PersistentCustomerSystem category: 'CustomerImporter'!
CustomerSystem subclass: #PersistentCustomerSystem
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!PersistentCustomerSystem methodsFor: 'start/close' stamp: 'f 11/20/2023 16:25:38'!
close
	session close! !

!PersistentCustomerSystem methodsFor: 'start/close' stamp: 'f 11/20/2023 16:21:16'!
initializeSession
	session:= (DataBaseSession for: (Array with: Address with: Customer))! !


!PersistentCustomerSystem methodsFor: 'testing' stamp: 'f 11/20/2023 17:00:31'!
numberOfCustomers
	^(session selectAllOfType: Customer) size! !


!PersistentCustomerSystem methodsFor: 'transaction/commit' stamp: 'f 11/20/2023 16:22:40'!
beginTransaction
	session beginTransaction! !

!PersistentCustomerSystem methodsFor: 'transaction/commit' stamp: 'f 11/20/2023 16:25:31'!
commit
	session commit! !


!PersistentCustomerSystem methodsFor: 'adding' stamp: 'f 11/20/2023 16:36:27'!
persist:anObject
	^session persist:anObject! !


!PersistentCustomerSystem methodsFor: 'access' stamp: 'f 11/20/2023 16:57:29'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ (session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !

!PersistentCustomerSystem methodsFor: 'access' stamp: 'f 11/20/2023 16:37:20'!
select: condition ofType: aType
	^session select: condition ofType:aType ! !

!PersistentCustomerSystem methodsFor: 'access' stamp: 'f 11/20/2023 16:36:19'!
selectAllOfType: aType
	^session selectAllOfType: aType! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PersistentCustomerSystem class' category: 'CustomerImporter'!
PersistentCustomerSystem class
	instanceVariableNames: ''!

!PersistentCustomerSystem class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 16:31:09'!
start
	^self new initializeSession! !


!classDefinition: #TransientCustomerSystem category: 'CustomerImporter'!
CustomerSystem subclass: #TransientCustomerSystem
	instanceVariableNames: 'customers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!TransientCustomerSystem methodsFor: 'start/close' stamp: 'f 11/20/2023 16:40:22'!
close
	! !

!TransientCustomerSystem methodsFor: 'start/close' stamp: 'f 11/20/2023 17:01:37'!
initializeCustomers
	customers:=OrderedCollection new.! !


!TransientCustomerSystem methodsFor: 'testing' stamp: 'f 11/20/2023 16:52:31'!
numberOfCustomers
	^customers size! !


!TransientCustomerSystem methodsFor: 'access' stamp: 'f 11/20/2023 17:10:08'!
customerWithIdentificationType: anIdType number: anIdNumber
^(customers
		detect: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]).
		! !


!TransientCustomerSystem methodsFor: 'adding' stamp: 'f 11/20/2023 16:42:32'!
persist:anObject
 	customers add: anObject
	! !


!TransientCustomerSystem methodsFor: 'transaction/commit' stamp: 'f 11/20/2023 16:40:18'!
beginTransaction
	! !

!TransientCustomerSystem methodsFor: 'transaction/commit' stamp: 'f 11/20/2023 16:40:26'!
commit
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransientCustomerSystem class' category: 'CustomerImporter'!
TransientCustomerSystem class
	instanceVariableNames: ''!

!TransientCustomerSystem class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:01:15'!
start
	^self new initializeCustomers ! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id inTransaction closed'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 11/14/2023 08:52:25'!
beginTransaction

	inTransaction := true.! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 11/14/2023 08:52:18'!
commit

	inTransaction := false.! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 11/14/2023 08:52:30'!
close

	closed := true.! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:49:30'!
assertCanUseDatabase

	self assertIsOpen.
	self assertInTransaction ! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:48:43'!
assertInTransaction

	inTransaction ifFalse: [ self error: 'Not in transaction' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:48:16'!
assertIsOpen

	closed ifTrue: [ self error: 'Connection with database closed' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 09:09:07'!
assertTypeIsPersisted: aType

	(configuration includes: aType) ifFalse: [ self error: 'Object of type ', aType name, ' are not configured to be persisted' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'f 11/20/2023 17:29:21'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:59'!
isRelationToPersist: possibleRelation

	^ possibleRelation class = Set! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:59'!
persistPossibleRelationOf: anObject at: anInstVarOffset

	| possibleRelation |
		
	possibleRelation := anObject instVarAt: anInstVarOffset.
	(self isRelationToPersist: possibleRelation) ifTrue: [ self persistRelationOf: anObject at: anInstVarOffset with: possibleRelation ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:03'!
persistRelationOf: anObject at: anInstVarOffset with: aRelation

	| persistentRelation |
	
	persistentRelation := PersistentSet on: self from: aRelation.
	anObject instVarAt: anInstVarOffset put: persistentRelation! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:34'!
persistRelationsOf: anObject

	anObject class instVarNamesAndOffsetsDo: [ :anInstVarName :anInstVarOffset | self persistPossibleRelationOf: anObject at: anInstVarOffset]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 09:06:24'!
tableOfType: aType

	^ tables at: aType ifAbsentPut: [ Set new ]! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:44:19'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.
	inTransaction := false.
	closed := false.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 11/14/2023 09:06:24'!
persist: anObject

	| table |

	self assertCanUseDatabase.
	self assertTypeIsPersisted: anObject class.
	self delay.
	
	table := self tableOfType: anObject class.
	self defineIdOf: anObject.
	table add: anObject.
	self persistRelationsOf: anObject.
! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 11/14/2023 09:06:56'!
select: aCondition ofType: aType

	self assertCanUseDatabase.
	self assertTypeIsPersisted: aType.
	self delay.
	
	^(self tableOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 11/14/2023 09:07:12'!
selectAllOfType: aType

	self assertCanUseDatabase.
	self assertTypeIsPersisted: aType.
	self delay.
	
	^(self tableOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !


!classDefinition: #Development category: 'CustomerImporter'!
Object subclass: #Development
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!


!classDefinition: #Enviroment category: 'CustomerImporter'!
Object subclass: #Enviroment
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Enviroment class' category: 'CustomerImporter'!
Enviroment class
	instanceVariableNames: ''!

!Enviroment class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:21:22'!
create
	^(self subclasses detect:[:aSubclass| aSubclass canHandle:(Smalltalk at: #ENV )]) create.! !


!classDefinition: #DevelopmentEnviroment category: 'CustomerImporter'!
Enviroment subclass: #DevelopmentEnviroment
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DevelopmentEnviroment class' category: 'CustomerImporter'!
DevelopmentEnviroment class
	instanceVariableNames: ''!

!DevelopmentEnviroment class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:18:35'!
canHandle: anEnviroment
	^anEnviroment='dev'.! !

!DevelopmentEnviroment class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:21:07'!
create
	^TransientCustomerSystem start.! !


!classDefinition: #IntegrationEnviroment category: 'CustomerImporter'!
Enviroment subclass: #IntegrationEnviroment
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IntegrationEnviroment class' category: 'CustomerImporter'!
IntegrationEnviroment class
	instanceVariableNames: ''!

!IntegrationEnviroment class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:18:51'!
canHandle: anEnviroment
	^anEnviroment='int'.! !

!IntegrationEnviroment class methodsFor: 'as yet unclassified' stamp: 'f 11/20/2023 17:20:45'!
create
	^PersistentCustomerSystem start.! !
