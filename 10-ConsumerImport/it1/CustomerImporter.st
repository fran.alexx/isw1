!classDefinition: #PersistentSet category: 'CustomerImporter'!
Set subclass: #PersistentSet
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!PersistentSet methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:25:31'!
initializeOn: aSession from: aNonPersistentSet

	session := aSession.
	self addAll: aNonPersistentSet ! !


!PersistentSet methodsFor: 'adding' stamp: 'HAW 11/14/2023 08:23:40'!
add: newObject

	super add: newObject.
	session persist: newObject.
	
	^newObject! !


!PersistentSet methodsFor: 'accessing' stamp: 'f 11/15/2023 15:30:23'!
select: aCondition
	^super select:aCondition! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PersistentSet class' category: 'CustomerImporter'!
PersistentSet class
	instanceVariableNames: ''!

!PersistentSet class methodsFor: 'instance creation' stamp: 'HAW 11/14/2023 08:24:32'!
on: aSession

	^self on: aSession from: #()! !

!PersistentSet class methodsFor: 'instance creation' stamp: 'HAW 11/14/2023 08:25:00'!
on: aSession from: aNonPersistentSet

	^self new initializeOn: aSession from: aNonPersistentSet
! !


!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 14:18:38'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
	(CustomerImport withSession: session forInput: 'input.txt' )importCustomers.! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 13:34:40'!
tearDown

	session commit.
	session close! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 14:19:02'!
test01ImportFindsAllCustomers
	|numberOfCustomers|
	
	numberOfCustomers:=(session selectAllOfType:Customer)size.
	self assert: numberOfCustomers equals:2.! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 14:19:07'!
test02ImportFindsAllAddresses
	|numberOfAddresses|

	numberOfAddresses:=(session selectAllOfType:Address)size.
	self assert: numberOfAddresses equals:3.! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 15:02:17'!
test03ImportedCustomersHaveTheirInformation
	|customers|
	
	customers:=self selectClientByID: 'D' IDNumber: '22333444'.
	
	self assert: customers size equals:1.
	self assertCustomerIsPepe:customers anyOne. 
	
	customers:=self selectClientByID: 'C' IDNumber: '23-25666777-9'.
	
	self assert: customers size equals:1.
	self assertCustomerIsJuan:customers anyOne.. ! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 14:54:52'!
test04ImportedAddressesHaveTheirInformation
	|addresses|
	
	addresses:=self selectAddressByName: 'San Martin'.
	
	self assert: addresses size equals:1.
	self assertIsSanMartinAddress: addresses anyOne. 
	
	addresses:=self selectAddressByName: 'Maipu'.
	
	self assert: addresses size equals:1.
	self assertIsMaipuAddress: addresses anyOne. 
	
	addresses:=self selectAddressByName: 'Alem'.
	
	self assert: addresses size equals:1.
	self assertIsAlemAddress: addresses anyOne. 
	
	
	
! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 15:33:58'!
test05ImportedCustomersHaveAllOfTheirAddresses
	|customers customerAdresses |
	
	customers:=self selectClientByID: 'D' IDNumber: '22333444'.
	customerAdresses:=customers anyOne addresses.
	
 	self assert:  customerAdresses size equals: 2.

	customers:=self selectClientByID: 'C' IDNumber: '23-25666777-9'.
	customerAdresses:=customers anyOne addresses.
	
	self assert: customerAdresses size equals:1.
	self assertIsAlemAddress: customerAdresses anyOne! !

!ImportTest methodsFor: 'test' stamp: 'f 11/15/2023 15:36:12'!
test06SearchingInvalidCustomerReturnsEmptyResult
	|customers|
	
	customers:=self selectClientByID: 'D' IDNumber: '2233344'.

 	self assert:  customers isEmpty.

	! !


!ImportTest methodsFor: 'assertions' stamp: 'f 11/15/2023 14:44:25'!
assertCustomerIsJuan:aCustomer
	self assert: (aCustomer firstName = (self customerJuan at:2)).
	self assert: (aCustomer lastName = (self customerJuan at:3)).
	self assert: (aCustomer identificationType = (self customerJuan at:4)).
	self assert: (aCustomer identificationNumber = (self customerJuan at:5)).! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/15/2023 14:43:11'!
assertCustomerIsPepe:aCustomer
	self assert: (aCustomer firstName = (self customerPepe at:2)).
	self assert: (aCustomer lastName = (self customerPepe at:3)).
	self assert: (aCustomer identificationType = (self customerPepe at:4)).
	self assert: (aCustomer identificationNumber = (self customerPepe at:5)).! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/15/2023 15:15:38'!
assertIsAlemAddress: anAddress
	self assert: (anAddress streetName = (self addressAlem at:2)).
	self assert: (anAddress streetNumber= (self addressAlem at:3)).
	self assert: (anAddress town = (self addressAlem at:4)).
	self assert: (anAddress zipCode = (self addressAlem at:5)).
	self assert: (anAddress province = (self addressAlem at:6)).
	! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/15/2023 15:15:25'!
assertIsMaipuAddress: anAddress
	self assert: (anAddress streetName = (self addressMaipu at:2)).
	self assert: (anAddress streetNumber= (self addressMaipu at:3)).
	self assert: (anAddress town = (self addressMaipu at:4)).
	self assert: (anAddress zipCode = (self addressMaipu at:5)).
	self assert: (anAddress province = (self addressMaipu at:6)).! !

!ImportTest methodsFor: 'assertions' stamp: 'f 11/15/2023 15:15:34'!
assertIsSanMartinAddress: anAddress
	self assert: (anAddress streetName = (self addressSanMartin at:2)).
	self assert: (anAddress streetNumber= (self addressSanMartin at:3)).
	self assert: (anAddress town = (self addressSanMartin at:4)).
	self assert: (anAddress zipCode = (self addressSanMartin at:5)).
	self assert: (anAddress province = (self addressSanMartin at:6)).
	! !


!ImportTest methodsFor: 'abstractions' stamp: 'f 11/15/2023 15:01:10'!
addressAlem
	|info|

	info:=OrderedCollection with:'A' with:'Alem' with: 1122with: 'CABA' with:1001 with:'CABA'.
	^info! !

!ImportTest methodsFor: 'abstractions' stamp: 'f 11/15/2023 15:01:03'!
addressMaipu
	|info|

	info:=OrderedCollection with:'A' with:'Maipu' with: 888with: 'Florida' with:1122 with:'Buenos Aires'.
	^info! !

!ImportTest methodsFor: 'abstractions' stamp: 'f 11/15/2023 15:00:55'!
addressSanMartin
	|info|

	info:=OrderedCollection with:'A' with:'San Martin' with: 3322with: 'Olivos' with:1636 with:'BsAs'.
	^info! !

!ImportTest methodsFor: 'abstractions' stamp: 'f 11/15/2023 14:41:43'!
customerJuan
	|info|
	info:=OrderedCollection with:'C' with:'Juan' with: 'Perez'with: 'C' with:'23-25666777-9'.
	^info! !

!ImportTest methodsFor: 'abstractions' stamp: 'f 11/15/2023 14:41:46'!
customerPepe
	|info|
	info:=OrderedCollection with:'C' with:'Pepe' with: 'Sanchez'with: 'D' with:'22333444'.
	^info! !


!ImportTest methodsFor: 'testing' stamp: 'f 11/15/2023 14:53:41'!
selectAddressByName: anAddressName

	^ session select:[:anAddress|anAddress streetName =anAddressName] ofType:Address! !

!ImportTest methodsFor: 'testing' stamp: 'f 11/15/2023 14:23:27'!
selectClientByID: anIDType IDNumber: anIDNumber 

	^ session select:[:aCustomer|aCustomer identificationType =anIDType and:[aCustomer 
			identificationNumber =anIDNumber]] ofType:Customer! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:25:42'!
initialize

	super initialize.
	addresses := Set new.! !


!classDefinition: #CustomerImport category: 'CustomerImporter'!
Object subclass: #CustomerImport
	instanceVariableNames: 'session input'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImport methodsFor: 'initialization' stamp: 'f 11/15/2023 14:03:20'!
initializeWithSession: aSession forInput: anAnInput
	session := aSession.
	input := anAnInput.! !


!CustomerImport methodsFor: 'evaluating' stamp: 'f 11/15/2023 14:09:36'!
importCustomers

	"
	self importCustomers
	"
	| inputStream  newCustomer line |

	inputStream := UniFileStream new open: input forWrite: false.

	line := inputStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth.
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth ].

		line := inputStream nextLine. ].


	inputStream close.
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImport class' category: 'CustomerImporter'!
CustomerImport class
	instanceVariableNames: ''!

!CustomerImport class methodsFor: 'instance creation' stamp: 'f 11/15/2023 14:03:20'!
withSession: aSession forInput: anAnInput
	^self new initializeWithSession: aSession forInput: anAnInput! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id inTransaction closed'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 11/14/2023 08:52:25'!
beginTransaction

	inTransaction := true.! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 11/14/2023 08:52:18'!
commit

	inTransaction := false.! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 11/14/2023 08:52:30'!
close

	closed := true.! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:49:30'!
assertCanUseDatabase

	self assertIsOpen.
	self assertInTransaction ! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:48:43'!
assertInTransaction

	inTransaction ifFalse: [ self error: 'Not in transaction' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:48:16'!
assertIsOpen

	closed ifTrue: [ self error: 'Connection with database closed' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 09:09:07'!
assertTypeIsPersisted: aType

	(configuration includes: aType) ifFalse: [ self error: 'Object of type ', aType name, ' are not configured to be persisted' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:59'!
isRelationToPersist: possibleRelation

	^ possibleRelation class = Set! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:59'!
persistPossibleRelationOf: anObject at: anInstVarOffset

	| possibleRelation |
		
	possibleRelation := anObject instVarAt: anInstVarOffset.
	(self isRelationToPersist: possibleRelation) ifTrue: [ self persistRelationOf: anObject at: anInstVarOffset with: possibleRelation ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:03'!
persistRelationOf: anObject at: anInstVarOffset with: aRelation

	| persistentRelation |
	
	persistentRelation := PersistentSet on: self from: aRelation.
	anObject instVarAt: anInstVarOffset put: persistentRelation! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:34'!
persistRelationsOf: anObject

	anObject class instVarNamesAndOffsetsDo: [ :anInstVarName :anInstVarOffset | self persistPossibleRelationOf: anObject at: anInstVarOffset]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 09:06:24'!
tableOfType: aType

	^ tables at: aType ifAbsentPut: [ Set new ]! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:44:19'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.
	inTransaction := false.
	closed := false.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 11/14/2023 09:06:24'!
persist: anObject

	| table |

	self assertCanUseDatabase.
	self assertTypeIsPersisted: anObject class.
	self delay.
	
	table := self tableOfType: anObject class.
	self defineIdOf: anObject.
	table add: anObject.
	self persistRelationsOf: anObject.
! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 11/14/2023 09:06:56'!
select: aCondition ofType: aType

	self assertCanUseDatabase.
	self assertTypeIsPersisted: aType.
	self delay.
	
	^(self tableOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 11/14/2023 09:07:12'!
selectAllOfType: aType

	self assertCanUseDatabase.
	self assertTypeIsPersisted: aType.
	self delay.
	
	^(self tableOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
