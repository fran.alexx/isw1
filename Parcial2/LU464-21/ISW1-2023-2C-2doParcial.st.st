!classDefinition: #MineGameTest category: 'ISW1-2023-2C-2doParcial.st'!
TestCase subclass: #MineGameTest
	instanceVariableNames: 'game'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!MineGameTest methodsFor: 'assertions' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
assertPlayerIsAt: anExpectedPosition withRemainingLives: anExpectedAmountOfLives havingClearedCells: aCollectionOfPositionsExpectedToBeClear inState: anExpectedStateClosure.

	self assert: (game playerPositionIs: anExpectedPosition).
	self assert: (game playerHasRemainingLives: anExpectedAmountOfLives).

	aCollectionOfPositionsExpectedToBeClear do: [:aPosition | self deny: (game hasSpecialCellsAt: aPosition)].

	self assert: anExpectedStateClosure value

	! !


!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 19:31:09'!
test01CannotCreateGameWithLessThanTwoColumns
	
	self should: [MineGame withBoard: 1@2 startingIn: 1@2 lifes: 2 specialsAt: {}	]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame invalidBoardDimentionsErrorDescription.
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 19:31:09'!
test02CannotCreateGameWithLessThanTwoRows
	
	self should: [MineGame withBoard: 2@1 startingIn: 1@1 lifes: 2 specialsAt: {}		]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame invalidBoardDimentionsErrorDescription.
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 19:31:09'!
test03PlayerCannotStartOutsideBoardDimentionsFromTheRight
		
	self should: [MineGame withBoard: 5@5 startingIn: 6@1 lifes: 2 specialsAt: {}]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame invalidStartingPosition.! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 19:31:09'!
test04PlayerCannotStartOutsideBoardDimentionsFromTheLeft
		
	self should: [MineGame withBoard: 5@5 startingIn: 0@1 lifes: 2 specialsAt: {}]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame invalidStartingPosition.! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 19:31:09'!
test05PlayerCannotStartInARowOtherThanFirst
		
	self should: [MineGame withBoard: 5@5 startingIn: 1@2 lifes: 2 specialsAt: {}]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame invalidStartingPosition.! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test06PlayerCanMoveToTheLeftInEmptyCell
	
	game := MineGame withBoard: 5@5 startingIn: 2@1 lifes: 2 specialsAt: {}.
	
	game moveLeft.
	
	self assert: (game playerPositionIs: 1@1).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test07PlayerCanMoveToTheRightInEmptyCell
		
	game := MineGame withBoard: 5@5 startingIn: 2@1 lifes: 2 specialsAt: {}.
	
	game moveRight.
	
	self assert: (game playerPositionIs: 3@1).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test08PlayerMovingToTheRightInTheEdgeOfBoardStaysInTheSamePosition
		
	game := MineGame withBoard: 5@5 startingIn: 5@1 lifes: 2 specialsAt: {}.
	
	game moveRight.
	
	self assert: (game playerPositionIs: 5@1).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test09PlayerMovingToTheLeftInTheEdgeOfBoardStaysInTheSamePosition
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: {}.
	
	game moveLeft.
	
	self assert: (game playerPositionIs: 1@1).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test10PlayerMovingDownInTheEdgeOfBoardStaysInTheSamePosition
		
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: {}.
	
	game moveDown.
	
	self assert: (game playerPositionIs: 1@1).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test10_2_PlayerCanDownInEmptyCell
		
	game := MineGame withBoard: 5@5 startingIn: 2@1 lifes: 2 specialsAt: {}.
	
	game moveUp ; moveDown.
	
	self assert: (game playerPositionIs: 2@1).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test11PlayerCanMoveUpInEmptyCell
	
		
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: {}.
	
	game moveUp.
	
	self assert: (game playerPositionIs: 1@2).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test12PlayerLosesOneLifeAfterSteppingOnSmallMineAndMineIsGoneFromTheBoard
	|bonusCells|
	
	bonusCells := OrderedCollection new.
	bonusCells add: (SmallMine atPosition: (1@2)).
	
		
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp.
	
	self assert: (game playerPositionIs: 1@2).
	self assert: (game playerHasRemainingLives: 1).
	self deny: (game hasSpecialCellsAt: 1@2).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test13PlayerWithArmorStaysTheSameAfterSteppingOnSmallMineAndMineIsGoneFromTheBoard
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (LightArmor atPosition: 1@2).
	bonusCells add: (SmallMine atPosition: 1@3).
		
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp.
	game moveUp.
	
	self assert: (game playerPositionIs: 1@3).
	self assert: (game playerHasRemainingLives: 2).
	self deny: (game hasSpecialCellsAt: 1@2).
	self deny: (game hasSpecialCellsAt: 1@3).
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 20:14:39'!
test14CannotCreateGameWithMultipleSpecialCellsInTheSamePosition
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (LightArmor atPosition: 1@2).
	bonusCells add: (SmallMine atPosition: 1@2).
	
	self should: [MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.		]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame positionWithMultipleBonusCelssErrorDescription.
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 20:07:06'!
test15CannotCreateGameWithSpecialCellsOutOfBoardBoundriesFromTheTop
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (LightArmor atPosition: 6@6).
	
	self should: [MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.		]
	raise: Error - MessageNotUnderstood
	withMessageText:  MineGame invalidSpecialCellPositionErrorDescription.
	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test16PlayerWithLightArmorThatFindsLightArmorReplacesArmor
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (LightArmor atPosition: 1@2).
	bonusCells add: (LightArmor atPosition: 1@3).
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp ; moveUp.
	
	self assertPlayerIsAt: 1@3 
		withRemainingLives: 2 
		havingClearedCells: {(1@2). (1@3)} 
		inState: [game lightArmorOn].
	

	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test17PlayerWithLightArmorThatFindsHeavyArmorReplacesArmor
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (LightArmor atPosition: 1@2).
	bonusCells add: (HeavyArmor atPosition: 1@3).
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp ; moveUp.
	
	self assertPlayerIsAt: 1@3 
		withRemainingLives: 2 
		havingClearedCells: {(1@2). (1@3)} 
		inState: [game heavyArmorOn].
	
	

	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test18PlayerWithHeavyArmorThatFindsLightArmorReplacesArmor
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (HeavyArmor atPosition: 1@2).
	bonusCells add: (LightArmor atPosition: 1@3).
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp ; moveUp.
	
	self assertPlayerIsAt: 1@3 
		withRemainingLives: 2 
		havingClearedCells: {(1@2). (1@3)} 
		inState: [game lightArmorOn].

	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test19PlayerWithHeavyArmorThatStepsOnMinesRetainLivesAndHeavyArmor
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (HeavyArmor atPosition: 1@2).
	bonusCells add: (SmallMine atPosition: 1@3).
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp ; moveUp.
	
	self assertPlayerIsAt: 1@3 
		withRemainingLives: 2 
		havingClearedCells: {(1@2). (1@3)} 
		inState: [game heavyArmorOn].
	

	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test20PlayerWithHeavyArmorThatStepsOnTwoMinesRetainLivesButLosesArmor
	|bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (HeavyArmor atPosition: 1@2).
	bonusCells add: (SmallMine atPosition: 1@3).
	bonusCells add: (SmallMine atPosition: 2@3).
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	
	game moveUp ; moveUp ; moveRight.
	
	self assertPlayerIsAt: 2@3 
		withRemainingLives: 2 
		havingClearedCells: {(1@2). (1@3)} 
		inState: [game noArmorState].
	
	

	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test21CannotMoveAgainAfterWinning

	game := MineGame withBoard: 2@2 startingIn: 1@1 lifes: 2 specialsAt: OrderedCollection new.
	
	game moveUp.
		
	self should: [game moveDown.		]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [:anError | 
		self assert: anError messageText equals: MineGame noMovesAfterGameEndedErrorDescrtiption.
		
		self assertPlayerIsAt: 1@2 
		withRemainingLives: 2 
		havingClearedCells: {} 
		inState: [game gameEnded].
	]
	
	

	! !

!MineGameTest methodsFor: 'tests' stamp: 'Franco Alesso 11/30/2023 21:46:19'!
test22PlayerThatStepsOnTunnelAppearsOnTheOtherEnd

	| bonusCells |
	
	bonusCells := OrderedCollection new.
	bonusCells add: (Tunnel atPosition: 1@2 to: 1@4).
	
	game := MineGame withBoard: 5@5 startingIn: 1@1 lifes: 2 specialsAt: bonusCells.
	game moveUp.
		
	self assertPlayerIsAt: 1@4 
		withRemainingLives: 2 
		havingClearedCells: {} 
		inState: [game noArmorState].
	
	

	! !


!classDefinition: #MineGame category: 'ISW1-2023-2C-2doParcial.st'!
Object subclass: #MineGame
	instanceVariableNames: 'currentPosition boardMaxDimentions remainingLives specialCellsPositions state'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!MineGame methodsFor: 'initialization' stamp: 'Franco Alesso 11/30/2023 20:30:51'!
initializeWithBoard: boardDimentions startingIn: aStartingPostion lives: aStartingAmountOfLives minesAt: aCollectionOfSmallMinePositions   
	boardMaxDimentions := boardDimentions.
	currentPosition := aStartingPostion.
	remainingLives := aStartingAmountOfLives.
	specialCellsPositions := aCollectionOfSmallMinePositions.
	state := BasicState of: self.! !


!MineGame methodsFor: 'movement' stamp: 'Franco Alesso 11/30/2023 21:29:37'!
endgame
	state := EndedState of: self.! !

!MineGame methodsFor: 'movement' stamp: 'Franco Alesso 11/30/2023 21:37:24'!
moveDown
	
	self moveToPosition: ((currentPosition - (0@1)) max: (currentPosition x@1)).
	! !

!MineGame methodsFor: 'movement' stamp: 'Franco Alesso 11/30/2023 21:36:45'!
moveLeft	
	self moveToPosition: ((currentPosition - (1@0)) max: (1@currentPosition y)).! !

!MineGame methodsFor: 'movement' stamp: 'Franco Alesso 11/30/2023 21:36:24'!
moveRight
	
	self moveToPosition: ((currentPosition + (1@0)) min: (boardMaxDimentions x@currentPosition y))
	
! !

!MineGame methodsFor: 'movement' stamp: 'Franco Alesso 11/30/2023 21:37:41'!
moveToPosition: aPosition

	self assertGameHasNotEnded. 
	
	currentPosition := aPosition.
	
	self moveThroughSpecialCells.! !

!MineGame methodsFor: 'movement' stamp: 'Franco Alesso 11/30/2023 21:37:08'!
moveUp

	self moveToPosition: currentPosition + (0@1).
	! !


!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:30:51'!
gameEnded
	^state gameEnded! !

!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:30:51'!
hasSpecialCellsAt: anExpectedSmallMinePosition 
	^specialCellsPositions anySatisfy: [:aSmallMinePosition | aSmallMinePosition = anExpectedSmallMinePosition]. ! !

!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:23:13'!
heavyArmorOn
	^state heavyArmorOn! !

!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:19:03'!
lightArmorOn
	^state lightArmorOn! !

!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:00:37'!
noArmorState
	^state noArmorState! !

!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 18:26:18'!
playerHasRemainingLives: anExpectedAmountOfLives
	 ^remainingLives = anExpectedAmountOfLives
	! !

!MineGame methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 18:04:30'!
playerPositionIs: anExpectedPosition
	^currentPosition = anExpectedPosition. 
	! !


!MineGame methodsFor: 'bonus cells' stamp: 'Franco Alesso 11/30/2023 21:44:51'!
checkIfInWinningPosition

	^ (currentPosition y = boardMaxDimentions y) ifTrue: [self endgame]! !

!MineGame methodsFor: 'bonus cells' stamp: 'Franco Alesso 11/30/2023 18:44:29'!
decreaseLives

	^ remainingLives := remainingLives -1.! !

!MineGame methodsFor: 'bonus cells' stamp: 'Franco Alesso 11/30/2023 21:44:51'!
moveThroughSpecialCells
	specialCellsPositions 
		detect: [:aSpecialCell | (aSpecialCell inPosition: currentPosition)]
		ifFound: [	:aSpecialCell | 
			aSpecialCell actionForState: state. 
			specialCellsPositions remove: aSpecialCell] 
		ifNone: [].
		
	self checkIfInWinningPosition.
		! !


!MineGame methodsFor: 'bonus - private' stamp: 'Franco Alesso 11/30/2023 20:34:48'!
activateNewHeavyArmor
	state := HeavyArmorState of: self.! !

!MineGame methodsFor: 'bonus - private' stamp: 'Franco Alesso 11/30/2023 20:35:03'!
activateNewSmallArmor
	state := SmallArmorState of: self.! !


!MineGame methodsFor: 'state - private' stamp: 'Franco Alesso 11/30/2023 19:42:43'!
returnToBasicState
	state := BasicState of: self.! !


!MineGame methodsFor: 'assertions' stamp: 'Franco Alesso 11/30/2023 21:30:41'!
assertGameHasNotEnded
	(self gameEnded) ifTrue: [self error: self class noMovesAfterGameEndedErrorDescrtiption]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MineGame class' category: 'ISW1-2023-2C-2doParcial.st'!
MineGame class
	instanceVariableNames: ''!

!MineGame class methodsFor: 'error description' stamp: 'Franco Alesso 11/30/2023 17:41:19'!
invalidBoardDimentionsErrorDescription
	 ^'Both board dimentionsMustBePositive'! !

!MineGame class methodsFor: 'error description' stamp: 'Franco Alesso 11/30/2023 20:07:35'!
invalidSpecialCellPositionErrorDescription
	^'There cannot be any specials outside of the board boundries'! !

!MineGame class methodsFor: 'error description' stamp: 'Franco Alesso 11/30/2023 17:37:09'!
invalidStartingPosition
	^'Starting position should be positive and within dimentions of the board'! !

!MineGame class methodsFor: 'error description' stamp: 'Franco Alesso 11/30/2023 21:11:06'!
noMovesAfterGameEndedErrorDescrtiption
	^'Game has already ended, no more moves allowed'! !

!MineGame class methodsFor: 'error description' stamp: 'Franco Alesso 11/30/2023 19:59:16'!
positionWithMultipleBonusCelssErrorDescription
	^'There cannot be two or more specials in the same position'! !


!MineGame class methodsFor: 'instance creation' stamp: 'Franco Alesso 11/30/2023 20:05:30'!
assertThereIsNoSpecialCellsInTheSamePosition: aCollectionOfSpecialCells

	^ aCollectionOfSpecialCells combinations: 2 atATimeDo: [:specialCells | 
		(specialCells first assertIsNotinSamePositionsAs: specialCells second).
	]! !

!MineGame class methodsFor: 'instance creation' stamp: 'Franco Alesso 11/30/2023 20:09:33'!
withBoard: board startingIn: aStartingPostion lifes: anAmountOfLives specialsAt: aCollectionOfSpecialCells   
	self assertBoardDimentionsAreValid: board.
	self assertIsValid: aStartingPostion withBoardDimentions: board.
	self assertThereIsNoSpecialCellsInTheSamePosition: aCollectionOfSpecialCells.
	
	aCollectionOfSpecialCells do: [:aSpecialCell | aSpecialCell assertIsInsideBoardWithDimentions: board].
	
	^self new initializeWithBoard: board startingIn: aStartingPostion lives: anAmountOfLives minesAt: aCollectionOfSpecialCells ! !


!MineGame class methodsFor: 'assertions' stamp: 'Franco Alesso 11/30/2023 18:14:13'!
assertBoardDimentionsAreValid: board 
	(board x < 2 or: [board y < 2]) ifTrue: [self error: self invalidBoardDimentionsErrorDescription].! !

!MineGame class methodsFor: 'assertions' stamp: 'Franco Alesso 11/30/2023 18:17:26'!
assertIsValid: aStartingPostion withBoardDimentions: board

	^ ((aStartingPostion between: 1@1 and: board) and: [aStartingPostion y = 1]) ifFalse: [self error: self invalidStartingPosition]! !


!classDefinition: #MineGameState category: 'ISW1-2023-2C-2doParcial.st'!
Object subclass: #MineGameState
	instanceVariableNames: 'game'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!MineGameState methodsFor: 'initialization' stamp: 'Franco Alesso 11/30/2023 19:25:25'!
initializeFor: aMineGame
	game := aMineGame.! !



!MineGameState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:28:59'!
gameEnded

	self subclassResponsibility! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MineGameState class' category: 'ISW1-2023-2C-2doParcial.st'!
MineGameState class
	instanceVariableNames: ''!

!MineGameState class methodsFor: 'instance creation' stamp: 'Franco Alesso 11/30/2023 19:23:31'!
of: aMineGame 
	^self new initializeFor: aMineGame ! !


!classDefinition: #EndedState category: 'ISW1-2023-2C-2doParcial.st'!
MineGameState subclass: #EndedState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!EndedState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:24:57'!
gameEnded
	^true.! !


!classDefinition: #PlayingState category: 'ISW1-2023-2C-2doParcial.st'!
MineGameState subclass: #PlayingState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!PlayingState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 21:43:46'!
actionForHeavyArmor
	game activateNewHeavyArmor.! !

!PlayingState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 21:22:21'!
actionForSmallArmor
	game activateNewSmallArmor.! !

!PlayingState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 21:29:13'!
actionForSmallMine

	self subclassResponsibility! !

!PlayingState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 21:40:10'!
actionForTunnel: anEndingPosition 
	game moveToPosition: anEndingPosition.! !


!PlayingState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:24:41'!
gameEnded
	^false! !

!PlayingState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:28:34'!
heavyArmorOn

	self subclassResponsibility! !

!PlayingState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:28:29'!
lightArmorOn

	self subclassResponsibility! !

!PlayingState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 21:28:45'!
noArmorState

	self subclassResponsibility! !


!classDefinition: #BasicState category: 'ISW1-2023-2C-2doParcial.st'!
PlayingState subclass: #BasicState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!BasicState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 19:26:17'!
actionForSmallMine
	game decreaseLives.! !


!BasicState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:24:08'!
heavyArmorOn
	^false.! !

!BasicState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:20:02'!
lightArmorOn
	^false.! !

!BasicState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:59:57'!
noArmorState
	^true.! !


!classDefinition: #HeavyArmorState category: 'ISW1-2023-2C-2doParcial.st'!
PlayingState subclass: #HeavyArmorState
	instanceVariableNames: 'durability'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!HeavyArmorState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:22:59'!
heavyArmorOn
	^true.! !

!HeavyArmorState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:23:42'!
lightArmorOn
	^false.! !

!HeavyArmorState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:59:00'!
noArmorState
	^false.! !


!HeavyArmorState methodsFor: 'initialization' stamp: 'Franco Alesso 11/30/2023 20:43:29'!
initializeFor: aGame
	durability := 2.
	game := aGame.! !

!HeavyArmorState methodsFor: 'initialization' stamp: 'Franco Alesso 11/30/2023 20:38:57'!
intializeFor: aGame
	durability := 2.
	game := aGame.! !


!HeavyArmorState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 21:00:06'!
actionForSmallMine
	durability := durability -1.
	
	(durability = 0) ifTrue: [game returnToBasicState].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeavyArmorState class' category: 'ISW1-2023-2C-2doParcial.st'!
HeavyArmorState class
	instanceVariableNames: ''!

!HeavyArmorState class methodsFor: 'as yet unclassified' stamp: 'Franco Alesso 11/30/2023 20:43:18'!
of: aa 
	^self new initializeFor: aa ! !


!classDefinition: #SmallArmorState category: 'ISW1-2023-2C-2doParcial.st'!
PlayingState subclass: #SmallArmorState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!SmallArmorState methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 19:38:01'!
actionForSmallMine
	game returnToBasicState.! !


!SmallArmorState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:24:47'!
heavyArmorOn
	^false.! !

!SmallArmorState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:19:23'!
lightArmorOn
	^true.! !

!SmallArmorState methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:59:51'!
noArmorState
	^false.! !


!classDefinition: #SpecialCells category: 'ISW1-2023-2C-2doParcial.st'!
Object subclass: #SpecialCells
	instanceVariableNames: 'boardPosition position'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!SpecialCells methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 19:31:51'!
actionWhenBasic: aBasicState

	self subclassResponsibility! !


!SpecialCells methodsFor: 'initialization' stamp: 'Franco Alesso 11/30/2023 19:50:31'!
initializeAtPosition: aBoardPosition 
	position := aBoardPosition.! !


!SpecialCells methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 19:53:44'!
inPosition: anExpectedPosition
	^position = anExpectedPosition.
	! !

!SpecialCells methodsFor: 'testing' stamp: 'Franco Alesso 11/30/2023 20:01:55'!
position
	^position! !


!SpecialCells methodsFor: 'assertions' stamp: 'Franco Alesso 11/30/2023 20:11:00'!
assertIsInsideBoardWithDimentions: board 
	(position between: 1@1 and: board) ifFalse: [self error: MineGame invalidSpecialCellPositionErrorDescription]! !

!SpecialCells methodsFor: 'assertions' stamp: 'Franco Alesso 11/30/2023 20:03:47'!
assertIsNotinSamePositionsAs: anotherSpecialCell 
	 (anotherSpecialCell inPosition: position )ifTrue: [self error: MineGame positionWithMultipleBonusCelssErrorDescription]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SpecialCells class' category: 'ISW1-2023-2C-2doParcial.st'!
SpecialCells class
	instanceVariableNames: ''!

!SpecialCells class methodsFor: 'instance creation' stamp: 'Franco Alesso 11/30/2023 19:50:16'!
atPosition: aBoardPosition 
	^self new initializeAtPosition: aBoardPosition ! !


!classDefinition: #HeavyArmor category: 'ISW1-2023-2C-2doParcial.st'!
SpecialCells subclass: #HeavyArmor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!HeavyArmor methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 20:21:06'!
actionForState: aState 
	aState actionForHeavyArmor.! !


!classDefinition: #LightArmor category: 'ISW1-2023-2C-2doParcial.st'!
SpecialCells subclass: #LightArmor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!LightArmor methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 19:42:07'!
actionForState: aState 
	aState actionForSmallArmor! !


!classDefinition: #SmallMine category: 'ISW1-2023-2C-2doParcial.st'!
SpecialCells subclass: #SmallMine
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!SmallMine methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 19:41:07'!
actionForState: aState 
	aState actionForSmallMine! !


!classDefinition: #Tunnel category: 'ISW1-2023-2C-2doParcial.st'!
SpecialCells subclass: #Tunnel
	instanceVariableNames: 'startingBoardPosition endingPosition'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-2C-2doParcial.st'!

!Tunnel methodsFor: 'initialization' stamp: 'Franco Alesso 11/30/2023 21:34:36'!
initializeAtPosition: aStartingBoardPosition to: anEndingPosition 
	position := aStartingBoardPosition.
	endingPosition := anEndingPosition.! !


!Tunnel methodsFor: 'action' stamp: 'Franco Alesso 11/30/2023 21:39:14'!
actionForState: aState 
	aState actionForTunnel: endingPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Tunnel class' category: 'ISW1-2023-2C-2doParcial.st'!
Tunnel class
	instanceVariableNames: ''!

!Tunnel class methodsFor: 'instance creation' stamp: 'Franco Alesso 11/30/2023 21:34:24'!
atPosition: aStartingBoardPosition to: anEndingPosition 
	^self new initializeAtPosition: aStartingBoardPosition to: anEndingPosition ! !
