!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 11:27:29'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	self closure: [CustomerBook new addCustomerNamed: 'John Lennon'] 
	       executesInLessThan: (50 * millisecond)
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 11:41:03'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds
	|customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	customerBook addCustomerNamed: paulMcCartney.
	
	self closure: [customerBook removeCustomerNamed: paulMcCartney] 
	       executesInLessThan: (100 * millisecond).
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 11:54:52'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	self evaluateClosureAndFail: [ customerBook addCustomerNamed: ''] 
	ifNotRaisedExceptionType: Error 
	elseVerify:  [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ].! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 13:59:50'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self evaluateClosureAndFail: [ customerBook removeCustomerNamed: 'Paul McCartney']
	ifNotRaisedExceptionType: NotFound 
	elseVerify: [ :anError | self AssertOnlyCustomerOf: customerBook hasName: johnLennon]

! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 14:04:16'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self assertValuesFrom: customerBook 
			numberOfActiveCustomers: 0
			numberOfSuspendedCustomers: 1 
			numberOfTotalCustomers: 1.

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 13:56:22'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assertValuesFrom: customerBook 
			numberOfActiveCustomers: 0
			numberOfSuspendedCustomers: 0 
			numberOfTotalCustomers: 0.
			
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 14:02:35'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self evaluateClosureAndFail: [ customerBook suspendCustomerNamed: 'George Harrison']
	ifNotRaisedExceptionType: CantSuspend 
	elseVerify:  [ :anError | self AssertOnlyCustomerOf: customerBook hasName: johnLennon]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FA 9/5/2023 14:00:34'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self evaluateClosureAndFail: [customerBook suspendCustomerNamed: johnLennon]
	ifNotRaisedExceptionType: CantSuspend 
	elseVerify: [ :anError | self AssertOnlyCustomerOf: customerBook hasName: johnLennon ].
! !


!CustomerBookTest methodsFor: 'abstractions for testing' stamp: 'FA 9/5/2023 13:58:55'!
AssertOnlyCustomerOf: customerBook hasName: customerName
	self assert: customerBook numberOfCustomers = 1.
	self assert: (customerBook includesCustomerNamed: customerName)! !

!CustomerBookTest methodsFor: 'abstractions for testing' stamp: 'FA 9/5/2023 13:55:31'!
assertValuesFrom: aCustomerBook numberOfActiveCustomers: activeCustomers
numberOfSuspendedCustomers: suspendedCustomers numberOfTotalCustomers: totalCustomers
	
	self assert: activeCustomers equals: aCustomerBook numberOfActiveCustomers.
	self assert: suspendedCustomers equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: totalCustomers equals: aCustomerBook numberOfCustomers.
! !

!CustomerBookTest methodsFor: 'abstractions for testing' stamp: 'FA 9/5/2023 11:09:40'!
closure: aClosure executesInLessThan: time
	self assert: (self closureExecutionTimeInMiliseconds: 
	                        aClosure) < time.
	! !

!CustomerBookTest methodsFor: 'abstractions for testing' stamp: 'FA 9/5/2023 11:03:01'!
closureExecutionTimeInMiliseconds: aClosure
	|millisecondsBeforeRunning millisecondsAfterRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClosure value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^millisecondsAfterRunning - millisecondsBeforeRunning! !

!CustomerBookTest methodsFor: 'abstractions for testing' stamp: 'FA 9/5/2023 11:54:16'!
evaluateClosureAndFail: aTestClosure ifNotRaisedExceptionType: anExceptionType elseVerify: 
anExceptionHandlerClosure

	[aTestClosure value. self fail]
		on: anExceptionType 
		do: anExceptionHandlerClosure
		! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'orderedCollection abstractions' stamp: 'FA 9/5/2023 14:34:24'!
remove: anElement from: anOrderedCollection

	1 to: anOrderedCollection size do: 
	[ :index |
		anElement = (anOrderedCollection at: index)
			ifTrue: [
				anOrderedCollection removeAt: index.
				^ true
			] 
	].
	^ false! !


!CustomerBook methodsFor: 'customer management' stamp: 'f 9/6/2023 19:51:27'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'FA 9/5/2023 14:38:57'!
removeCustomerNamed: aName 
 
	(self remove: aName from: active) ifTrue: [^aName ] .
	(self remove: aName from: suspended) ifTrue: [^aName].
	
	^NotFound signal
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/4/2023 17:02:48'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/4/2023 17:02:52'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
