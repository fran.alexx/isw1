!classDefinition: #MarsRoverLogTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverLogTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 16:27:11'!
test01EmptyCommandReturnsEmptyLog
	|marsRover marsRoverLog|
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #position.
	
	marsRover process: ''.
	
	self assert: (marsRoverLog hasLog: OrderedCollection new). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 16:28:39'!
test02OneMovementCommandReturnsPosition
	|marsRover marsRoverLog expectedLog|
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #position.
	
	marsRover process: 'f'.
	
	expectedLog := OrderedCollection with: 1@2.
	
	self assert: (marsRoverLog hasLog: expectedLog). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 16:32:33'!
test03OneRotationCommandDoesNotAffectLog
	|marsRover marsRoverLog expectedLog|
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #position.
	
	marsRover process: 'r'.
	
	expectedLog := OrderedCollection new.
	
	self assert: (marsRoverLog hasLog: expectedLog). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 16:34:39'!
test04MultipleMixedCommandsRegisterCorrectly
	|marsRover marsRoverLog expectedLog|
	
	marsRover := MarsRover at: 0@0 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #position.
	
	marsRover process: 'frflf'.
	
	expectedLog := OrderedCollection with: 0@1 with: 1@1 with: 1@2.	
	self assert: (marsRoverLog hasLog: expectedLog). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 19:23:41'!
test05DirectionLogWithOneCommand

	|marsRover marsRoverLog expectedLog|
	
	marsRover := MarsRover at: 0@0 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #direction.
	
	marsRover process: 'r'.
	
	expectedLog := OrderedCollection with: 'East'.
	self assert: (marsRoverLog hasLog: expectedLog). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 19:24:27'!
test06DirectionLogWithMultipleMixedCommands
	|marsRover marsRoverLog expectedLog|
	
	marsRover := MarsRover at: 0@0 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #direction.
	
	marsRover process: 'rlf'.
	
	expectedLog := OrderedCollection with: 'East' with: 'North'.
	self assert: (marsRoverLog hasLog: expectedLog). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 19:24:47'!
test07MixedLogWithMultipleMixedCommands
	|marsRover marsRoverLog expectedLog|
	
	marsRover := MarsRover at: 0@0 heading: self north . 
	
	marsRoverLog:= MarsRoverLog of: marsRover mode: #mixed.
	
	marsRover process: 'frflf'.
	
	expectedLog := OrderedCollection with: 0@1 with: 'East' with: 1@1 with: 'North' with: 1@2.	
	
	self assert: (marsRoverLog hasLog: expectedLog). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 19:25:05'!
test08DifferentLogTypesInitializedAtDifferentTimes
	|marsRover logFromStart intermediateLog expectedLogForInitial expectedLogForIntermidiate|
	
	marsRover := MarsRover at: 0@0 heading: self north . 
	
	logFromStart := MarsRoverLog of: marsRover mode: #mixed.
	
	marsRover process: 'frflf'.
		
	intermediateLog := MarsRoverLog of: marsRover mode: #position.
	
	marsRover process: 'brf'.
	
	expectedLogForInitial := OrderedCollection new.
	expectedLogForInitial add: 0@1 ; add: 'East' ; add: 1@1 ; add: 'North' ; add: 1@2 ; add: 1@1 ; add: 'East' ; add: 2@1.	
	
	expectedLogForIntermidiate := OrderedCollection with: 1@1 with: 2@1.	

	
	self assert: (logFromStart hasLog: expectedLogForInitial). 
	
	self assert: (intermediateLog hasLog: expectedLogForIntermidiate). ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/29/2023 19:25:40'!
test09WindowPositionLogWithOneCommand
	|marsRover window expectedLog|
	
	marsRover := MarsRover at: 0@0 heading: self north . 
	
	window := MarsRoverWindow of: marsRover mode: #position.
	
	marsRover process: 'f'.
				
	expectedLog:=  OrderedCollection with: 0@1.	

	
	self assert: (window hasLog: expectedLog). 
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FA 10/30/2023 11:17:12'!
test10LogAndWindowCanCoexistWithCorrectObservations
	|marsRover window log expectedWindow expectedLog|
	
	marsRover := MarsRover at: 0@0 heading: self north.
	
	window := MarsRoverWindow of: marsRover mode: #mixed.
	
	log := MarsRoverLog of: marsRover mode: #mixed.
	
	marsRover process: 'ffrr'.
				
	expectedWindow:=  OrderedCollection with: 0@2 with: 'South'.	
	
	expectedLog := OrderedCollection with: 0@1 with: 0@2 with: 'East' with: 'South'.
	
	self assert: (window hasLog: expectedWindow). 
	
	self assert: (log hasLog: expectedLog). 
! !


!MarsRoverLogTest methodsFor: 'heading' stamp: 'FA 10/29/2023 15:45:37'!
north

	^ MarsRoverHeadingNorth ! !


!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'FA 10/29/2023 16:55:27'!
initializeAt: aPosition heading: aHeadingType

	position := MarsRoverPosition for: self at: aPosition.
	head := MarsRoverDirection for: self heading: (aHeadingType for: self). ! !


!MarsRover methodsFor: 'heading' stamp: 'FA 10/29/2023 18:44:21'!
headEast
	
	head change: (MarsRoverHeadingEast for: self)! !

!MarsRover methodsFor: 'heading' stamp: 'FA 10/29/2023 18:44:25'!
headNorth
	
	head change: (MarsRoverHeadingNorth for: self).! !

!MarsRover methodsFor: 'heading' stamp: 'FA 10/29/2023 18:44:28'!
headSouth
	
	head change: (MarsRoverHeadingSouth for: self)! !

!MarsRover methodsFor: 'heading' stamp: 'FA 10/29/2023 18:44:30'!
headWest
	
	head change: (MarsRoverHeadingWest for: self)! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'FA 10/29/2023 16:06:45'!
isAt: aPosition heading: aHeadingType

	^(position isAt: aPosition) and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'FA 10/29/2023 18:44:47'!
moveEast
	
	position change: (1@0)! !

!MarsRover methodsFor: 'moving' stamp: 'FA 10/29/2023 15:53:50'!
moveForward
	
	head moveForward
	
	! !

!MarsRover methodsFor: 'moving' stamp: 'FA 10/29/2023 18:45:01'!
moveNorth
	
	position change: (0@1).! !

!MarsRover methodsFor: 'moving' stamp: 'FA 10/29/2023 18:45:06'!
moveSouth
	
	position change: (0@-1)! !

!MarsRover methodsFor: 'moving' stamp: 'FA 10/29/2023 18:45:11'!
moveWest
	
	position change: (-1@0)! !


!MarsRover methodsFor: 'suscriptions' stamp: 'FA 10/29/2023 19:03:58'!
suscribeLog: aLog toSubject: initializationClosure.
	
	initializationClosure value.
	 ! !

!MarsRover methodsFor: 'suscriptions' stamp: 'FA 10/30/2023 11:12:35'!
suscribeLogtoDirection: aLog
	head attach: aLog.
	 ! !

!MarsRover methodsFor: 'suscriptions' stamp: 'FA 10/30/2023 11:12:35'!
suscribeLogtoPosition: aLog
	position attach: aLog.
	 ! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'printing' stamp: 'FA 10/29/2023 17:09:23'!
cardinalString
	^'East'! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'printing' stamp: 'FA 10/29/2023 17:09:29'!
cardinalString
	^'North'! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!MarsRoverHeadingSouth methodsFor: 'printing' stamp: 'FA 10/29/2023 17:09:34'!
cardinalString
	^'South'! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'printing' stamp: 'FA 10/29/2023 17:10:03'!
cardinalString
	'West'! !


!classDefinition: #MarsRoverObserver category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverObserver
	instanceVariableNames: 'publishers logInformation'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverObserver methodsFor: 'attachment' stamp: 'FA 10/29/2023 18:50:58'!
hasLog: anExpectedLog
	self subclassResponsibility! !

!MarsRoverObserver methodsFor: 'attachment' stamp: 'FA 10/30/2023 11:11:33'!
suscribeTo: aPublisher
	self subclassResponsibility! !

!MarsRoverObserver methodsFor: 'attachment' stamp: 'FA 10/29/2023 18:51:31'!
update: aPublisher
	self subclassResponsibility! !


!MarsRoverObserver methodsFor: 'initialization' stamp: 'FA 10/29/2023 19:22:48'!
initializeOf: marsRover inMode: aMode.
	publishers := OrderedCollection new.
	
	(aMode = #position) ifTrue: [marsRover suscribeLogtoPosition: self].
	(aMode = #direction) ifTrue: [marsRover suscribeLogtoDirection: self].
	
	(aMode = #mixed) ifTrue: [
		marsRover suscribeLogtoPosition: self.
		marsRover suscribeLogtoDirection: self
	].

	logInformation := OrderedCollection new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverObserver class' category: 'MarsRover-WithHeading'!
MarsRoverObserver class
	instanceVariableNames: ''!

!MarsRoverObserver class methodsFor: 'instance creation' stamp: 'FA 10/29/2023 19:21:35'!
of: marsRover mode: aMode
	^self new initializeOf: marsRover inMode: aMode. ! !


!classDefinition: #MarsRoverLog category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #MarsRoverLog
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverLog methodsFor: 'position' stamp: 'FA 10/29/2023 19:22:49'!
update: aPublisher
	logInformation add: aPublisher publish.! !


!MarsRoverLog methodsFor: 'testing' stamp: 'FA 10/29/2023 19:22:49'!
hasLog: aLog
	^logInformation = aLog.! !


!MarsRoverLog methodsFor: 'attachment' stamp: 'FA 10/30/2023 11:11:33'!
suscribeTo: aPublisher
	publishers add: aPublisher.
	! !


!classDefinition: #MarsRoverWindow category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #MarsRoverWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverWindow methodsFor: 'position' stamp: 'FA 10/29/2023 18:28:32'!
update: aPublisher
	! !


!MarsRoverWindow methodsFor: 'testing' stamp: 'FA 10/29/2023 19:22:49'!
hasLog: aLog
	
	logInformation := OrderedCollection new.
	
	publishers do: [:aPublisher | logInformation add: aPublisher publish].
	
	^logInformation = aLog.! !


!MarsRoverWindow methodsFor: 'attachment' stamp: 'FA 10/30/2023 11:11:34'!
suscribeTo: aPublisher
	publishers add: aPublisher.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverWindow class' category: 'MarsRover-WithHeading'!
MarsRoverWindow class
	instanceVariableNames: ''!

!MarsRoverWindow class methodsFor: 'instance creation' stamp: 'FA 10/29/2023 18:20:56'!
of: marsRover mode: aMode
	^self new initializeOf: marsRover inMode: aMode. ! !


!classDefinition: #MarsRoverSubject category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverSubject
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverSubject methodsFor: 'publish' stamp: 'FA 10/30/2023 11:12:52'!
attach: anObsever
	self subclassResponsibility.! !

!MarsRoverSubject methodsFor: 'publish' stamp: 'FA 10/29/2023 18:40:35'!
change: aSubject
	self subclassResponsibility.! !

!MarsRoverSubject methodsFor: 'publish' stamp: 'FA 10/29/2023 18:39:55'!
publish
	self subclassResponsibility.! !


!classDefinition: #MarsRoverDirection category: 'MarsRover-WithHeading'!
MarsRoverSubject subclass: #MarsRoverDirection
	instanceVariableNames: 'suscribers direction rover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverDirection methodsFor: 'rotate' stamp: 'FA 10/29/2023 18:45:31'!
change: aDirection
	direction := aDirection.
	
	suscribers do: [ :aSuscriber | aSuscriber update: self].! !

!MarsRoverDirection methodsFor: 'rotate' stamp: 'FA 10/29/2023 17:03:51'!
moveBackward
	direction moveBackward.! !

!MarsRoverDirection methodsFor: 'rotate' stamp: 'FA 10/29/2023 17:04:19'!
moveForward
	direction moveForward.! !

!MarsRoverDirection methodsFor: 'rotate' stamp: 'FA 10/29/2023 16:58:01'!
rotateLeft
	direction rotateLeft.! !

!MarsRoverDirection methodsFor: 'rotate' stamp: 'FA 10/29/2023 16:58:09'!
rotateRight
	direction rotateRight.! !


!MarsRoverDirection methodsFor: 'initialization' stamp: 'FA 10/29/2023 16:49:51'!
initalizeFor: aMarsRover heading: aDirection. 
	rover := aMarsRover.
	direction := aDirection.
	suscribers := OrderedCollection new.! !


!MarsRoverDirection methodsFor: 'suscribe' stamp: 'FA 10/30/2023 11:13:05'!
attach: anObserver
	anObserver suscribeTo: self.
	suscribers add: anObserver.! !


!MarsRoverDirection methodsFor: 'publising' stamp: 'FA 10/29/2023 18:12:27'!
publish
	^direction cardinalString.! !


!MarsRoverDirection methodsFor: 'testing' stamp: 'FA 10/29/2023 17:02:58'!
isHeading: aDirection
	^direction class = aDirection.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverDirection class' category: 'MarsRover-WithHeading'!
MarsRoverDirection class
	instanceVariableNames: ''!

!MarsRoverDirection class methodsFor: 'instance creation' stamp: 'FA 10/29/2023 16:49:26'!
for: aMarsRover heading: aDirection
	^self new initalizeFor: aMarsRover heading: aDirection. ! !


!classDefinition: #MarsRoverPosition category: 'MarsRover-WithHeading'!
MarsRoverSubject subclass: #MarsRoverPosition
	instanceVariableNames: 'rover position suscribers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverPosition methodsFor: 'position' stamp: 'FA 10/29/2023 18:45:52'!
change: coordinatesToAdd
	position := position + coordinatesToAdd.
	
	suscribers do: [:aSuscriber | aSuscriber update: self.]
	! !


!MarsRoverPosition methodsFor: 'initialization' stamp: 'FA 10/29/2023 16:19:32'!
initalizeFor: aMarsRover at: aPosition
	rover := aMarsRover.
	position := aPosition.
	suscribers := OrderedCollection new.! !


!MarsRoverPosition methodsFor: 'suscriptions' stamp: 'FA 10/30/2023 11:12:35'!
attach: aLog
	aLog suscribeTo: self.
	suscribers add: aLog.! !


!MarsRoverPosition methodsFor: 'testing' stamp: 'FA 10/29/2023 16:08:45'!
isAt: aPosition
	^position = aPosition.! !


!MarsRoverPosition methodsFor: 'publish' stamp: 'FA 10/29/2023 18:03:23'!
publish
	^position.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverPosition class' category: 'MarsRover-WithHeading'!
MarsRoverPosition class
	instanceVariableNames: ''!

!MarsRoverPosition class methodsFor: 'instance creation' stamp: 'FA 10/29/2023 16:01:13'!
for: aMarsRover at: aPosition
	^self new initalizeFor: aMarsRover at: aPosition. ! !
