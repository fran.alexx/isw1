!classDefinition: #PortfolioAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioAccountTest
	instanceVariableNames: 'accountWith1Deposit depositOf100 accountWith1Withdraw withdrawOf200'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/14/2023 16:06:11'!
setUp	
	accountWith1Deposit := ReceptiveAccount new.
	depositOf100 := Deposit register: 100 on: accountWith1Deposit.
	
	accountWith1Withdraw := ReceptiveAccount new.
	withdrawOf200 := Withdraw register: 200 on: accountWith1Withdraw.
	! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 15:11:27'!
test01EmptyPortfolioHasZeroBalance
	self assert: (PortfolioAccount new hasBalance: 0).! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 16:55:58'!
test02PorfolioWithOneReceptiveAccountHasItsBalance
	|portfolio |
	
	portfolio := PortfolioAccount new.
	portfolio add: accountWith1Deposit.
	
	self assert: (portfolio hasBalance: 100).
	! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 16:55:34'!
test03PorfolioWithMoreThanTwoReceptiveAccountsHasSumOfBalances
	|portfolio |
	
	portfolio := PortfolioAccount new.
	
	portfolio add: accountWith1Deposit ; add: accountWith1Withdraw.
	
	self assert: (portfolio hasBalance: -100).
	! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 15:11:27'!
test04PortfolioWithOnePortfolioHasPortfolioBalance
	|portfolio1 portfolio2 |
	
	portfolio2 := PortfolioAccount new.
	
	portfolio2 add: accountWith1Deposit ; add: accountWith1Withdraw.
	
	portfolio1 := PortfolioAccount new.
	portfolio1 add: portfolio2.
	
	self assert: (portfolio1 hasBalance: -100).
	! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 15:11:27'!
test05EmptyPortfolioHasNoTransactions
	
	self assert: (PortfolioAccount new transactions isEmpty).! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 16:55:04'!
test06PortfolioWithOnlyReceptiveAccountsHasTheirTransactions
	
	|portfolio |
	
	portfolio := PortfolioAccount new.
	
	portfolio add: accountWith1Deposit ; add: accountWith1Withdraw.
	
	self assert: (portfolio hasRegistered: depositOf100).
	self assert: (portfolio hasRegistered: withdrawOf200).

! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 16:54:42'!
test07MixedPortfolioHasTransactionsOfAllChildAccountGroups
	
	|mainPortfolio innerPortfolio|
	
	innerPortfolio := PortfolioAccount new.
	innerPortfolio add: accountWith1Withdraw.	
	
	mainPortfolio := PortfolioAccount new.
	mainPortfolio add: accountWith1Deposit.
	mainPortfolio add: innerPortfolio.
	
	self assert: (mainPortfolio hasRegistered: depositOf100).
	self assert: (mainPortfolio hasRegistered: withdrawOf200).

! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 16:54:32'!
test08MixedPortfolioReturnsFullTransactionList
	
	|mainPortfolio innerPortfolio transactions|
	
	innerPortfolio := PortfolioAccount new.
	innerPortfolio add: accountWith1Withdraw.	
	
	mainPortfolio := PortfolioAccount new.
	mainPortfolio add: accountWith1Deposit.
	mainPortfolio add: innerPortfolio.
	
	transactions := OrderedCollection with: depositOf100 with: withdrawOf200.
	
	self assert: mainPortfolio transactions equals: transactions.! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 15:11:27'!
test09PortfolioCannotHaveItselfAsAsset
	
	|mainPortfolio|
	
	self should:[
		mainPortfolio := PortfolioAccount new.
		mainPortfolio add: accountWith1Deposit.
		mainPortfolio add: mainPortfolio.]
	raise: Error
	withExceptionDo: [:anError | self assert:  PortfolioAccount invalidAssetErrorDescription equals: anError messageText].! !

!PortfolioAccountTest methodsFor: 'tests' stamp: 'FA 10/17/2023 15:11:27'!
test10PortfolioCannotHaveAccountThanIsAnotherAssetPortfolio
	
	|mainPortfolio innerPortfolio|
	
	mainPortfolio := PortfolioAccount new.
	mainPortfolio add: accountWith1Deposit.
	
	innerPortfolio := PortfolioAccount new.
	innerPortfolio add: accountWith1Deposit.
	
	self should:[ mainPortfolio add: innerPortfolio.]
	raise: Error
	withExceptionDo: [:anError | self assert:  PortfolioAccount invalidAssetErrorDescription equals: anError messageText].! !



!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'f 10/9/2023 20:41:37'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'FA 10/14/2023 16:35:07'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: ((Withdraw register: withdrawValue on: account) hasValue: 50).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/17/2021 17:29:53'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountGroup category: 'Portfolio-Ejercicio'!
Object subclass: #AccountGroup
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountGroup methodsFor: 'account information' stamp: 'FA 10/17/2023 16:09:12'!
balance
	self subclassResponsibility.! !

!AccountGroup methodsFor: 'account information' stamp: 'FA 10/17/2023 16:09:04'!
transactions
	self subclassResponsibility.! !


!AccountGroup methodsFor: 'account managment' stamp: 'FA 10/17/2023 16:08:53'!
add: anAssetToAdd
	self subclassResponsibility.! !


!AccountGroup methodsFor: 'assertions' stamp: 'FA 10/17/2023 16:39:18'!
assertIsNotInAccountGroupTree: anAssetToAdd
	self subclassResponsibility.
	! !


!AccountGroup methodsFor: 'testing' stamp: 'FA 10/17/2023 16:37:31'!
hasBalance: aBalance
	^self balance = aBalance.! !

!AccountGroup methodsFor: 'testing' stamp: 'FA 10/17/2023 16:10:37'!
hasRegister: aTransaction
	self subclassResponsibility.! !


!classDefinition: #PortfolioAccount category: 'Portfolio-Ejercicio'!
AccountGroup subclass: #PortfolioAccount
	instanceVariableNames: 'childAccountGroups'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioAccount methodsFor: 'initialization' stamp: 'FA 10/17/2023 16:41:00'!
initialize
	childAccountGroups := OrderedCollection new.! !


!PortfolioAccount methodsFor: 'account information' stamp: 'FA 10/17/2023 16:41:00'!
balance
	^childAccountGroups sum: [:anAsset | anAsset balance] ifEmpty: [0].! !

!PortfolioAccount methodsFor: 'account information' stamp: 'FA 10/17/2023 16:41:00'!
transactions
	|transactions|
	
	transactions := OrderedCollection new.
	
	childAccountGroups do: [:anAsset | transactions addAllLast: anAsset transactions].
		
	^transactions.! !


!PortfolioAccount methodsFor: 'account managment' stamp: 'FA 10/17/2023 16:41:00'!
add: anAccountGroup
	self assertIsNotInAccountGroupTree: anAccountGroup.
	childAccountGroups add: anAccountGroup.! !


!PortfolioAccount methodsFor: 'assertions' stamp: 'FA 10/17/2023 16:42:50'!
assertIsNotInAccountGroupTree: anAssetToAdd	
	(self isCurrentPortfolioOrItsIncludedInChildAccountGroup: anAssetToAdd) ifTrue: [^ self error: self class invalidAssetErrorDescription]. 
	
	^true.
		
	! !

!PortfolioAccount methodsFor: 'assertions' stamp: 'FA 10/17/2023 16:41:46'!
hasMutualExlusionWithChildAccountGroups: anAccountGroupToAdd
	^childAccountGroups allSatisfy: [ :anAccountGroup | 
		(anAccountGroupToAdd assertIsNotInAccountGroupTree: anAccountGroup) or:
		[anAccountGroup assertIsNotInAccountGroupTree: anAccountGroupToAdd]]
	
	
	! !

!PortfolioAccount methodsFor: 'assertions' stamp: 'FA 10/17/2023 16:42:50'!
isCurrentPortfolioOrItsIncludedInChildAccountGroup: anAccountGroup
	^self = anAccountGroup or: [(self hasMutualExlusionWithChildAccountGroups: anAccountGroup) not] ! !


!PortfolioAccount methodsFor: 'testing' stamp: 'FA 10/17/2023 16:41:00'!
hasRegistered: aTransaction 
	^childAccountGroups anySatisfy: [:anAsset | anAsset hasRegistered: aTransaction]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PortfolioAccount class' category: 'Portfolio-Ejercicio'!
PortfolioAccount class
	instanceVariableNames: ''!

!PortfolioAccount class methodsFor: 'error descriptions' stamp: 'FA 10/14/2023 16:56:10'!
invalidAssetErrorDescription
	'Cicle detected in assets'! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
AccountGroup subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'NR 10/17/2019 15:06:56'!
initialize

	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'account information' stamp: 'FA 10/17/2023 16:16:02'!
balance
	^transactions inject:0 into:[:aBalance :aTransaction| aTransaction appliedToBalance: aBalance]
	! !

!ReceptiveAccount methodsFor: 'account information' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'account managment' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !


!ReceptiveAccount methodsFor: 'assertions' stamp: 'FA 10/17/2023 16:39:19'!
assertIsNotInAccountGroupTree: anAsset
	^(self = anAsset) not.! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'FA 10/17/2023 16:51:55'!
appliedToBalance: aBalance

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'value' stamp: 'FA 10/14/2023 16:37:05'!
hasValue: aTransactionValue
	^aTransactionValue = value.! !


!AccountTransaction methodsFor: 'initalization' stamp: 'FA 10/14/2023 16:37:35'!
initializeFor: aValue

	value := aValue.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'value' stamp: 'FA 10/17/2023 16:16:01'!
appliedToBalance:aBalance

	^aBalance+value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'value' stamp: 'FA 10/17/2023 16:16:01'!
appliedToBalance:aBalance

	^aBalance+(value*-1)! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !
