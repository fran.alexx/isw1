!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 09:09:37'!
test01PrefixCannotBeEmpty
	self
		should: [SentenceFinderByPrefix with: '' on: self emptyStack]
		raise: Error
		withExceptionDo: [:anError | 
			self assert: anError messageText equals: SentenceFinderByPrefix emptyPrefixErrorDescription]! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 09:41:25'!
test02PrefixCannotHaveSpace
	self
		should: [SentenceFinderByPrefix with: ' I am a prefix with spaces ' on: self emptyStack]
		raise: Error
		withExceptionDo: [:anError | 
			self assert: anError messageText equals: SentenceFinderByPrefix prefixWithSpacesErrorDescription]! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 09:15:45'!
test03NoSentencesFoundOnEmptyStack
	|sentenceFinder foundSentences emptyStack|
	
	emptyStack := self emptyStack.
	
	sentenceFinder := SentenceFinderByPrefix with: 'Hello' on: emptyStack.
	foundSentences := sentenceFinder find.
	
	self assert: emptyStack isEmpty.
	self assert: foundSentences isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 09:33:30'!
test04DoesNotFindSentencesThatDoNotIncludePrefix
	|sentenceFinder foundSentences aStack 	sentencesToFind|
	
	sentencesToFind := OrderedCollection with: 'this deserves at least a 9' with: 'Doesn´t it?'.  
	aStack := self createStackWithSentences: sentencesToFind.
	
	sentenceFinder := SentenceFinderByPrefix with: 'these' on: aStack.
	foundSentences := sentenceFinder find.
	
	self assert: aStack hasSameElementsInIdenticalOrderThan: (self createStackWithSentences: sentencesToFind).
	self assert: foundSentences isEmpty.
	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 09:32:52'!
test05DoesNotFindSentencesThatHavePrefixInPlaceOtherThanStart
	|sentenceFinder foundSentences aStack 	sentencesToFind|
	
	sentencesToFind := OrderedCollection with: 'an animal' with: 'what a beuatiful animal!!' with: 'the animal'.  
	aStack := self createStackWithSentences: sentencesToFind.
	
	sentenceFinder := SentenceFinderByPrefix with: 'animal' on: aStack.
	foundSentences := sentenceFinder find.
	
	self assert: aStack hasSameElementsInIdenticalOrderThan: (self createStackWithSentences: sentencesToFind).
	self assert: foundSentences isEmpty.
	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 10:27:04'!
test06FindsAllSentencesThatStartWithPrefix
	|sentenceFinder foundSentences aStack 	sentencesToFind|
	
	sentencesToFind := OrderedCollection with: 'Argentina ' with: 'Argelia'  with: 'Argon'.  
	aStack := self createStackWithSentences: sentencesToFind.
	
	sentenceFinder := SentenceFinderByPrefix with: 'Arg' on: aStack.
	foundSentences := sentenceFinder find.
	
	self assert: aStack hasSameElementsInIdenticalOrderThan: (self createStackWithSentences: sentencesToFind).
	self assert: foundSentences equals: sentencesToFind reverse.! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'F 9/17/2023 10:03:11'!
test07FindIsCaseSensitive
	|sentenceFinder foundSentences aStack 	sentencesToFind|
	
	sentencesToFind := OrderedCollection with: 'Argentina ' with: 'Argelia'  with: 'Argon'.  
	aStack := self createStackWithSentences: sentencesToFind.
	
	sentenceFinder := SentenceFinderByPrefix with: 'arg' on: aStack.
	foundSentences := sentenceFinder find.
	
	self assert: aStack hasSameElementsInIdenticalOrderThan: (self createStackWithSentences: sentencesToFind).
	self assert: foundSentences isEmpty.! !


!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'F 9/17/2023 09:39:35'!
assert: aStack hasIdenticalOrderAs: anotherStack
	|originalStackSize|
	originalStackSize := aStack size.
	
	originalStackSize timesRepeat: [self assert: aStack pop equals: anotherStack pop]! !

!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'F 9/17/2023 09:34:47'!
assert: aStack hasSameElementsInIdenticalOrderThan: anotherStack

	self assert: aStack hasSameSizeThan: anotherStack.
	self assert: aStack hasIdenticalOrderAs: anotherStack. 
	! !

!SentenceFinderByPrefixTest methodsFor: 'assertions' stamp: 'F 9/17/2023 09:39:45'!
assert: aStack hasSameSizeThan: anotherStack.

	self assert: aStack size equals: anotherStack size.
	! !


!SentenceFinderByPrefixTest methodsFor: 'test data' stamp: 'F 9/17/2023 09:09:02'!
createStackWithSentences: sentencesToPush
	|aStack|
	aStack := OOStack new.
	sentencesToPush do: [:aSentence | aStack push: aSentence].
	^aStack.! !

!SentenceFinderByPrefixTest methodsFor: 'test data' stamp: 'F 9/17/2023 09:04:47'!
emptyStack
	^OOStack new! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'testing' stamp: 'F 9/17/2023 15:56:23'!
isEmpty
	^contents isEmpty.
	
	! !

!OOStack methodsFor: 'testing' stamp: 'F 9/17/2023 16:07:24'!
isNotEmpty
	^contents notEmpty.
	
	! !


!OOStack methodsFor: 'initialization' stamp: 'F 9/16/2023 16:23:55'!
initialize 
	contents := OrderedCollection new ! !


!OOStack methodsFor: 'operations' stamp: 'F 9/17/2023 16:03:26'!
pop 
	|objectAtTheTop|
		
	objectAtTheTop := self top.
	contents removeLast.
	
	^objectAtTheTop
	
		! !

!OOStack methodsFor: 'operations' stamp: 'F 9/16/2023 17:21:00'!
push: anObject
	^contents add: anObject! !

!OOStack methodsFor: 'operations' stamp: 'F 9/16/2023 16:35:41'!
size 
	^contents size! !

!OOStack methodsFor: 'operations' stamp: 'F 9/17/2023 16:13:09'!
top	
	^self currentState top.! !


!OOStack methodsFor: 'operations - private' stamp: 'F 9/17/2023 15:50:11'!
currentState 
	^OOStackState stateOf: self.! !

!OOStack methodsFor: 'operations - private' stamp: 'F 9/17/2023 15:55:16'!
topIfEmpty
	^contents last.! !

!OOStack methodsFor: 'operations - private' stamp: 'F 9/17/2023 15:55:34'!
topIfNotEmpty
	^self error: self class stackEmptyErrorDescription.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/14/2023 08:12:21'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #OOStackState category: 'Stack-Exercise'!
Object subclass: #OOStackState
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackState methodsFor: 'initialization' stamp: 'F 9/17/2023 15:18:59'!
initializeFor: aStack
	stack := aStack! !


!OOStackState methodsFor: 'operations for stack' stamp: 'F 9/17/2023 15:20:18'!
top
	^self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackState class' category: 'Stack-Exercise'!
OOStackState class
	instanceVariableNames: ''!

!OOStackState class methodsFor: 'determine state' stamp: 'F 9/17/2023 15:45:59'!
isMyState: aStack
	self subclassResponsibility.! !

!OOStackState class methodsFor: 'determine state' stamp: 'F 9/17/2023 16:28:22'!
stateOf: aStack

	|subclassWhereStackBelongs|
	 
	subclassWhereStackBelongs := self subclasses detect: [: aStackState | aStackState isMyState: aStack].
	
	^subclassWhereStackBelongs of: aStack.! !


!OOStackState class methodsFor: 'instance creation' stamp: 'F 9/17/2023 16:09:25'!
of: aStack
	^self new initializeFor: aStack.! !


!classDefinition: #OOEmptyStackState category: 'Stack-Exercise'!
OOStackState subclass: #OOEmptyStackState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOEmptyStackState methodsFor: 'operations for stack' stamp: 'F 9/17/2023 16:00:11'!
top
	^stack topIfNotEmpty.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOEmptyStackState class' category: 'Stack-Exercise'!
OOEmptyStackState class
	instanceVariableNames: ''!

!OOEmptyStackState class methodsFor: 'determine state' stamp: 'F 9/17/2023 15:48:06'!
isMyState: aStack
	^aStack isEmpty.
	! !


!classDefinition: #OONotEmptyStackState category: 'Stack-Exercise'!
OOStackState subclass: #OONotEmptyStackState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OONotEmptyStackState methodsFor: 'operations for stack' stamp: 'F 9/17/2023 15:53:01'!
top
	^stack topIfEmpty! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OONotEmptyStackState class' category: 'Stack-Exercise'!
OONotEmptyStackState class
	instanceVariableNames: ''!

!OONotEmptyStackState class methodsFor: 'determine state' stamp: 'F 9/17/2023 15:59:18'!
isMyState: aStack
	^aStack isNotEmpty.! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'prefix stack originalStackSize reversedStack foundSentences'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'F 9/16/2023 20:21:43'!
initializeWith: aPrefix on: aStack
	prefix := aPrefix.
	stack := aStack.
	originalStackSize := stack size.
	foundSentences := OrderedCollection new.
	reversedStack := OOStack new. ! !


!SentenceFinderByPrefix methodsFor: 'sentence finding - private' stamp: 'F 9/16/2023 19:29:04'!
addToFoundSentencesIfStartsWithPrefix: aSentence
	(aSentence beginsWith: prefix) ifTrue: [foundSentences add: aSentence].! !

!SentenceFinderByPrefix methodsFor: 'sentence finding - private' stamp: 'F 9/16/2023 19:28:08'!
analizeSentenceAtTheTopOfTheStack
	|aSentence|
	
	aSentence := stack pop.
	self addToFoundSentencesIfStartsWithPrefix: aSentence.
	reversedStack push: aSentence.! !

!SentenceFinderByPrefix methodsFor: 'sentence finding - private' stamp: 'F 9/16/2023 19:30:51'!
findAllSentencesStartingWithPrefixFromStack
	(originalStackSize) timesRepeat: [self analizeSentenceAtTheTopOfTheStack].! !

!SentenceFinderByPrefix methodsFor: 'sentence finding - private' stamp: 'F 9/16/2023 19:32:54'!
pushBackElementFromReversedStack
	stack push: reversedStack pop.! !

!SentenceFinderByPrefix methodsFor: 'sentence finding - private' stamp: 'F 9/16/2023 19:34:22'!
restoreOriginalStack
	(originalStackSize) timesRepeat: [self pushBackElementFromReversedStack].! !


!SentenceFinderByPrefix methodsFor: 'sentence finding' stamp: 'F 9/17/2023 10:15:09'!
find	
	self findAllSentencesStartingWithPrefixFromStack.
	self restoreOriginalStack.
	^foundSentences.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'F 9/16/2023 20:14:44'!
emptyPrefixErrorDescription
        ^'Empty prefix is not allowed'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'F 9/16/2023 20:12:18'!
prefixWithSpacesErrorDescription
      ^'Prefix with spaces are not allowed'! !


!SentenceFinderByPrefix class methodsFor: 'assertions' stamp: 'F 9/16/2023 20:17:02'!
assertIsNotEmpty: aPrefix
	(aPrefix isEmpty) ifTrue: [self error: self emptyPrefixErrorDescription].! !

!SentenceFinderByPrefix class methodsFor: 'assertions' stamp: 'F 9/16/2023 20:17:41'!
assertThereAreNoSpaces: aPrefix
	(aPrefix includes: $ ) ifTrue: [self error: self prefixWithSpacesErrorDescription].! !


!SentenceFinderByPrefix class methodsFor: 'instance creation' stamp: 'F 9/16/2023 20:18:25'!
with: aPrefix on: aStack

	self assertIsNotEmpty: aPrefix.
	self assertThereAreNoSpaces: aPrefix.
	
	^self new initializeWith: aPrefix on: aStack.! !
