!classDefinition: #I category: '00-NumerosNaturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: '00-NumerosNaturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: '00-NumerosNaturales'!
I class
	instanceVariableNames: 'next'!

!I class methodsFor: 'Axioms' stamp: 'f 8/27/2023 09:14:45'!
* aNaturalNumber
    ^aNaturalNumber! !

!I class methodsFor: 'Axioms' stamp: 'FA 8/24/2023 21:02:56'!
+ aNaturalNumber
     ^ aNaturalNumber next! !

!I class methodsFor: 'Axioms' stamp: 'f 8/31/2023 22:15:48'!
/ aNaturalNumber
      aNaturalNumber = I ifTrue: [	^self].
	^self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor! !

!I class methodsFor: 'Axioms' stamp: 'FA 8/24/2023 21:04:13'!
next
	next ifNotNil: [^next].
	next := self cloneNamed: self name, 'I'.
	^next.! !


!I class methodsFor: 'Error Control' stamp: 'f 8/31/2023 22:15:04'!
- aNaturalNumber 
	self error: self descripcionDeErrorDeNumerosNegativosNoSoportados! !

!I class methodsFor: 'Error Control' stamp: 'f 8/30/2023 20:05:44'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
      ^'El resultado de dividir por un numero mayor es un racional. No soportado en nuestro modelo'! !

!I class methodsFor: 'Error Control' stamp: 'f 8/30/2023 18:37:23'!
descripcionDeErrorDeNumerosNegativosNoSoportados 
          ^'El modelo no soporta numeros negativos'! !

!I class methodsFor: 'Error Control' stamp: 'f 8/31/2023 22:14:32'!
errorrr: mensajeDeError
      ^mensajeDeError! !


!I class methodsFor: 'Expanded Arithmetic' stamp: 'f 9/1/2023 16:11:03'!
substractTo: unNatural
	^unNatural previous! !


!I class methodsFor: 'Comparison' stamp: 'f 8/27/2023 11:24:13'!
< aNaturalNumber
    (aNaturalNumber = I) ifTrue: [^false].
    ^true.
	! !


!I class methodsFor: '--** private fileout/in **--' stamp: 'f 9/1/2023 17:02:47'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := II.! !


!classDefinition: #II category: '00-NumerosNaturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: '00-NumerosNaturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: '00-NumerosNaturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'Axioms' stamp: 'F 8/28/2023 21:50:01'!
next
	next ifNotNil: [^next].
	next := II createChildNamed: self name, 'I'.
	next previous: self.
	^next.! !

!II class methodsFor: 'Axioms' stamp: 'FA 8/24/2023 21:22:17'!
previous
	^previous! !


!II class methodsFor: 'Arithmetic' stamp: 'f 8/27/2023 09:37:37'!
* aNaturalNumber
	^aNaturalNumber + (self previous * aNaturalNumber)! !

!II class methodsFor: 'Arithmetic' stamp: 'FA 8/24/2023 21:14:20'!
+ aNaturalNumber
     ^ self previous + aNaturalNumber next! !

!II class methodsFor: 'Arithmetic' stamp: 'f 9/1/2023 16:21:22'!
- aNaturalNumber
	^aNaturalNumber substractTo: self! !

!II class methodsFor: 'Arithmetic' stamp: 'f 9/1/2023 16:19:40'!
/ aNaturalNumber
	(self < aNaturalNumber) ifTrue: [^self error: II descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	(self = aNaturalNumber) ifTrue: [^I].
	(I = aNaturalNumber) ifTrue: [^self].
       ^self timesICanSubstract: aNaturalNumber.! !


!II class methodsFor: 'Comparison' stamp: 'f 9/1/2023 16:20:03'!
< aNaturalNumber
	^self isDescendantOf: aNaturalNumber
	! !

!II class methodsFor: 'Comparison' stamp: 'f 9/1/2023 17:02:00'!
isDescendantOf: aNaturalNumber
        (aNaturalNumber = I) ifTrue: [^false].
        (aNaturalNumber previous = self) ifTrue: [^true].
	^self isDescendantOf: aNaturalNumber previous
	! !


!II class methodsFor: 'Error Control' stamp: 'f 8/30/2023 20:04:32'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
      ^'El resultado de dividir por un numero mayor es un racional. No soportado en nuestro modelo'! !


!II class methodsFor: 'Expanded Arithmetic' stamp: 'f 9/1/2023 16:21:39'!
substractTo: aNaturalNumber
	^ aNaturalNumber previous - self previous! !

!II class methodsFor: 'Expanded Arithmetic' stamp: 'f 9/1/2023 16:13:28'!
timesICanSubstract: aNaturalNumber
	(aNaturalNumber = self) ifTrue: [^I].
	^ I + ((self - aNaturalNumber) timesICanSubstract: aNaturalNumber).! !


!II class methodsFor: 'Creation Settings' stamp: 'FA 8/24/2023 21:24:13'!
previous: aNaturalNumber
	previous := aNaturalNumber! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'f 9/1/2023 17:02:47'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: '00-NumerosNaturales'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: '00-NumerosNaturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: '00-NumerosNaturales'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'f 9/1/2023 17:02:47'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !


!classDefinition: #IIII category: '00-NumerosNaturales'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: '00-NumerosNaturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: '00-NumerosNaturales'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: '--** private fileout/in **--' stamp: 'f 9/1/2023 17:02:47'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := III.! !

I initializeAfterFileIn!
II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!